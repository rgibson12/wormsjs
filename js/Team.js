Team = function (game, context, sprite, wormCount, colour, id){
	this._wormCount = 2;
	//this._wormCount = wormCount;
	this._worms = [];
	//temp 
	this.i = 0;
	this.sprite = sprite;
	this.teamColour = colour;
	this.id = id;
	this.createTeam(game, context);	
	
	//console.log("TEAM ID: " + this.id);
	
};
	Team.prototype = {
		createTeam: function(game, context){
			for (var i = 0; i < this._wormCount; i++){
				var worm = new Worm(Math.floor((Math.random() * 250) + 1) , 50, game, context, this.sprite, this.teamColour, this.id);
				this._worms.push(
					worm);
			}
			return this._worms[0];
		},
		
		alignHpLabels: function(){
			for (var i =0; i < this._worms.length; i++){
				this._worms[i].getHpLabel().x = this._worms[i].getX()-1.5;
				this._worms[i].getHpLabel().y = this._worms[i].getY() - 25;
			}
		},
		
		getWorm: function(i){
			return this._worms[i];
		},
		
		getWholeTeam: function(){
			return this._worms;
		}
	};