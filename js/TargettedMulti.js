TargettedMulti = function(id, sprite, power, splash, shotsAllowed, fuse, explosionRadius, weaponType,range, targetable, spacing){
		this._id = id;
		this._sprite = sprite;
		this._power = power;
		this.bulletExists = false;
		this.bullets=[];
		this._splash = splash;
		this._shotsAllowed = shotsAllowed;
		this._fuse = fuse;
		this._explosionRadius = explosionRadius;
		this._shotsInProgress = false;
		this._body;
		this._weaponType = weaponType;
		this._range = range;
		this._targetable = targetable;
		this._bulletCount = 4;
		this.fired = false;
		this.collidingBullet;
		this._charging = false;
		this.spacing = spacing;
		this._maxDamage = 25;
	};
	
	TargettedMulti.prototype = {
		getId: function(){
			return this._id;
		},
		
		getCharging(){
			return this._charging;
		},
		
		getTargetable(){
			return this._targetable;
		},
		
		getRange: function(){
			return this._range;
		},
		getWeaponType: function(){
			return this._weaponType;
		},
		getSprite: function(){
			return this._sprite;
		},
		getPower: function(){
			return this._power;
		},
		draw: function(game, x, y){
			//var weapon = game.add.sprite(x,y, this._sprite);
			////console.log(weapon.width);
			this._body = game.add.sprite(x,y, this._sprite);
			return this._body;
		},
		getWidth: function(vari){
			console.log(vari);
			return this._body.width;
		},
		getBody: function(){
			return this._body;
		},
		getShotsAllowed: function(){
			return this._shotsAllowed;
		},
		
		getFuse: function(){
			return this._fuse;
		},
		
		getExplosionRaidus: function(){
			return this._explosionRadius;
		},

		getShotsInProgress: function(){
			return this._shotsInProgress;
		},
		
		setShotsInProgress: function(shotsInProgress){
			this._shotsInProgress = shotsInProgress;
		},
		
		getBullets(){
			return this.bullets;
		},
		
		setBullets(bullets){
			this.bullets = bullets;
		},
		
		fire: function(args){
			this.bullets = [];
			this.bullets.length =0;
			//console.log("FIRING");
			
			var x = args[0], y = args[1], game = args[2], context = args[3], progressor = -(args[4]*1.5);
			//this.collidingBullet = null;
			game.input.mouse.enabled = false;
			this.bullets.splice(0, this.bullets.length);
			if(!this.bulletExists){
				//console.log("spawning bullets");
				this.bulletExists = true;
				var p = new Phaser.Point(x,y);
				
				for (var i = 0; i < this._bulletCount; i++){
					this.bullets.push(game.add.sprite(p.x+progressor, p.y+(progressor/4), 'bullet'));
					progressor+=args[4];
				}
				
				for (i = 0; i <this.bullets.length; i++){
					////console.log("we are: "+ this.bullets[i]);
										game.physics.box2d.enable(this.bullets[i]);
					this.bullets[i].body.setCircle(3);
					this.bullets[i].body.i = i;
					this.bullets[i].exploded = false;
					this.bullets[i].body.setCategoryContactCallback(2, (function(item){
						this.collidingBullet = this.bullets[item.i];
						this.collidingBullet.i = item.i;
						this.collidingBullet.collided = true;
						////console.log("setting colliding bullet as; " + item.i);
						context.worldCollideCallback();
					}), this);
					this.bullets[i].body.setCategoryContactCallback(3, (function(item){
						this.collidingBullet = this.bullets[item.i];
						this.collidingBullet.i = item.i;
						this.collidingBullet.collided = true;
						////console.log("setting colliding bullet as; " + item.i);
						context.worldCollideCallback();
					}), this);
					////console.log("pushing bullets");
					
				}
				context.hitOnce = false;
				context.firePower = 0;
				context.exploded = false;
				this.fired = true;
				//game.camera.follow(this.bullets[1]);
				
			}
		},
		
		pathBullet: function(){
			if (this.bulletExists){
				for (var i = 0; i < this.bullets.length; i++){
					this.bullets[i].body.rotaion = Math.atan2(this.bullets[i].body.velocity.y, this.bullets[i].body.velocity.x);
				}
			}
		},
		
		showTargetting: function(context,game){
			console.log("Adding target from weapon");
			context.targetting = undefined;
			context.targetting = game.add.sprite(context.input.x, context.input.y, 'targetCrosshair');
			context.targetting.anchor.setTo(0.5,0.5);
			game.input.keyboard.stop();
			game.camera.follow(context.targetting);
		},
		
		explodeBullet: function(context,game){
			context.timerInEffect = true;
			if (this.bullets[this.collidingBullet.i] != undefined){
			////console.log("EXPLODING this.collidingBullet FOR PROJECTILE  : " + this.collidingBullet.i);
				this.hitOnce = false;
				//console.log("NOW EXPLODING " + this.collidingBullet.i);
				//this.collidingBullet = this.bullets[this.collidingBullet];
				this.collidingBullet.kill();
				this.collidingBullet.destroy();
				this.bullets[this.collidingBullet.i].exploded = true;
				context.bulletHitLand = false;	
				var oldVertices = context.groundVertices;
				var p = new Phaser.Point(this.collidingBullet.x, this.collidingBullet.y);

				context.explosion = new Phaser.Circle(p.x,p.y, this.getExplosionRaidus());			
				var newCirc = context.createCircle(20, p, this.getExplosionRaidus()/2);
				
				var subj_paths = new ClipperLib.Paths();
				
				for(var x = 0; x < context.getCurrArrs().length; x++){
					var subj_path = new ClipperLib.Path();
					var toPush = context.getCurrArrs()[x];
					for (var i = 0; i< toPush.length; i+=2){
						subj_path.push(
							new ClipperLib.IntPoint(toPush[i], toPush[i+1]));
					}		
					subj_paths.push(subj_path);
					subj_path = null;
				}

				var clip_paths = new ClipperLib.Paths();
				var clip_path = new ClipperLib.Path();
				for(i=0; i < newCirc.length; i+=2){
					clip_path.push(
						new ClipperLib.IntPoint(Math.round(newCirc[i]), Math.round(newCirc[i+1])));
				}
				clip_paths.push(clip_path);
				var cpr = new ClipperLib.Clipper();
				
				var scale = 1;
				ClipperLib.JS.ScaleUpPaths(subj_paths, scale);
				ClipperLib.JS.ScaleUpPaths(clip_paths, scale);
				
				cpr.AddPaths(subj_paths, ClipperLib.PolyType.ptSubject, true);
				cpr.AddPaths(clip_paths, ClipperLib.PolyType.ptClip, true);
				var solution_paths =[];
				var clipType = ClipperLib.ClipType.ctDifference;
				var subject_fillType = ClipperLib.PolyFillType.pftNonZero;
				var clip_fillType = ClipperLib.PolyFillType.pftNonZero;
				var succeeded = cpr.Execute(clipType, solution_paths, subject_fillType, clip_fillType);
			
				var newBods = [];
				var newArrs = [];
				var toX, toY;
				context.groundBody.clearFixtures();
					if(this.getWeaponType() <= 1){
					for (i = 0; i < solution_paths.length; i++){
						var newTerrain = [];
						var poly = solution_paths[i];
						for(var j = 0; j < poly.length; j++){
							var vert = poly[j];
							if (j == 0){
								toX = vert.X; 
								toY = vert.Y;
							}
							newTerrain.push(vert.X);
							newTerrain.push(vert.Y);	
						}
						newTerrain.push(toX);
						newTerrain.push(toY);
						
						newBods.push(context.getGroundBody().addChain(newTerrain));	
						newArrs.push(newTerrain);
					}
					
					for (i = 0; i < context.getCurrBods().length; i++){
						context.getGroundBody().removeFixture(context.getCurrBods()[i]);
					}
					context.getCurrBods().splice(0, context.getCurrBods().length);
					context.getCurrArrs().splice(0, context.getCurrArrs().length);
					for (i = 0; i < newBods.length; i++){
						context.getCurrBods().push(newBods[i]);
					} 
					for (i = 0; i < newArrs.length; i++){
						context.getCurrArrs().push(newArrs[i]);
					}	
					context.getGroundBody().setCollisionCategory(2);
					
					var x = Math.floor(this.collidingBullet.x);
					var y = Math.floor(this.collidingBullet.y);
					var rgba = context.getLand().getPixel(x, y);
					
					context.getLand().blendDestinationOut();
					context.getLand().circle(x, y, this.getExplosionRaidus()/2, 'rgba(0, 0, 0, 255');
					context.getLand().blendReset();
					context.getLand().update();
					context.getLand().addToWorld();			
					//MAX DX = 600 -> -600; MAX DY  = 700 -> -700;
					}
					this.effectWorms(0.5, game, context);
					
					
					//context.activeWorm.setShotsTaken(context.activeWorm.getShotsTaken() + 1);
					context.hitOnce = true;
			
			//this.bulletExists=false;
			
			//this.bullets.splice[this.collidingBullet.i, 1];
			//this.bullets.length -=1;
			
			//context.exploded = true;
			//context.fuseTimer.destroy();
			//add delay
			game.camera.follow(context.activeWorm.body);			
			context.weaponPanel.bringToFront();
			
			}
		},
		checkBulletLocation(){
			if(this.fired){
				if(this.bullets.length != undefined && this.bullets.length > 0){
					//console.log(this.bullets.length + "length");
					for (var i = 0 ; i < this.bullets.length; i++){
						//console.log("this bullet has exploded?" + this.bullets[i].exploded);
									if (this.bullets[i].x > $(window).width()*2 || this.bullets[i].x < ($(window).width()*-2) || this.bullets[i].y > $(window).height()*2 || this.bullets[i].y < $(window).height()*-2){
										this.bullets[i].exploded=true;
										
									} 
									if(this.bullets[i].exploded !=true){
										//this.fired = false;
										return false;
										
									}
					}
					this.bulletExists = false;
					this.fired = false;
					this.bullets.splice(0,this.bullets.length);
					this.bullets = [];
					return true;
				}
				return false;
			}
		},
		
		effectWorms: function(multiplier, game, context){
			for (var i = 0; i < context.turnSystem.getTeams().length; i++){
				for (var j = 0; j < context.turnSystem.getTeams()[i].getWholeTeam().length; j++){
					if (context.turnSystem.getTeams()[i].getWholeTeam()[j].isAlive()){
					//console.log("WIDTH: " + context.turnSystem.getTeams()[i].getWholeTeam()[j].getHeight());
					var topLeftY = context.turnSystem.getTeams()[i].getWholeTeam()[j].getY() - (context.turnSystem.getTeams()[i].getWholeTeam()[j].getHeight()/2);
					var topLeftX = context.turnSystem.getTeams()[i].getWholeTeam()[j].getX()-(context.turnSystem.getTeams()[i].getWholeTeam()[j].getWidth()/2);
					var width = context.turnSystem.getTeams()[i].getWholeTeam()[j].getWidth();
					var height = context.turnSystem.getTeams()[i].getWholeTeam()[j].getHeight();
					var lines = [
						new Phaser.Line(topLeftX, topLeftY, topLeftX + width, topLeftY),
						new Phaser.Line(topLeftX, topLeftY,topLeftX, topLeftY+height),
						new Phaser.Line(topLeftX + width, topLeftY, topLeftX+width, topLeftY+height),
						new Phaser.Line(topLeftX, topLeftY + height, topLeftX+width, topLeftY+height)
					];
					
					var centerX = topLeftX + (width/2);
					var centerY = topLeftY + (height/2);
					
					var intersects;
					//console.log("explosion: " + context.explosion.x + ", "+ context.explosion.y + ".... radius: " + context.explosion.radius);
					for(var p = 0; p < lines.length; p++) {
						var ar = lines[p].coordinatesOnLine();
						game.debug.geom(lines[p],'rgba(255,255,0, 1)');
						//console.log(p+ ":");
						for (var x = 0; x < ar.length; x++){
							//console.log(ar[x][0] + ", " + ar[x][1]);
							intersects = context.explosion.contains(ar[x][0], ar[x][1]);
							if (intersects) {
								break;
							}
						}
						if (intersects) {
							break;
						}
					}
								// Find the closest intersection
								//dist = game.math.distance(context.explosion.x, context.explosion.y, intersect.x, intersect.y);
							/* }
							
				}						if (context.explosion.contains(context.turnSystem.getTeams()[i].getWholeTeam()[j].getX(), context.turnSystem.getTeams()[i].getWholeTeam()[j].getY())){ */
						//console.log("WORM IN EXPLOSION");
					if (intersects){
						var dx =  centerX-context.explosion.x ;
						var dy = centerY-context.explosion.y  ;
						//console.log(dx + ", " + dy);
						dx*=30;dy*=30;	
						context.firePower = 0;
						
			//TEMP - TODO: APPLY CALCULATED DAMAGE, NOT 50 **********************************************			
						var dist = Math.abs(context.explosion.distance(new Phaser.Point(centerX,centerY)));
							var distDed = (dist/context.explosion.radius);
							var dmg = Math.round(this._maxDamage - (this._maxDamage * distDed));
							context.turnSystem.getTeams()[i].getWholeTeam()[j].applyDamage(dmg*multiplier);
						//dx-=(dx*distDed);
						//dy-=(dy*distDed);
						
			//TEMP***************************************************************************************
						context.turnSystem.getTeams()[i].getWholeTeam()[j].body.body.velocity.x = dx-(dx*distDed);
						context.turnSystem.getTeams()[i].getWholeTeam()[j].body.body.velocity.y= dy-(dy*distDed);
			//TEMP***************************************************************************************
					}
				}
				}
			}
		},
	};