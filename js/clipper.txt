   // var game = new Phaser.Game(640, 480, Phaser.CANVAS, 'game');

    BasicGame.Game = function (game) {

		this.groundVertices = [-200, 300, -200,-0,-130.128,5.08323,-89.0526,-0.105309,-31.5464,1.19183,1.31426,5.5156,
		50.6053,4.21846,87.7897,-2.26719,148.755,-4.42908,200,-0,251.899,4.99306,
		289.063,13.0285,335.267,18.0508,369.418,9.01079,383.48,14.033,410.6,3.9886,
		433.702,2.98414,467.853,10.0152,487.942,20.0596,512.048,23.073,545.195,8.00633,
		571.31,1.97973,591.399,-16.1001,623.541,-24.1356,647.647,-20.1179,678.785,-22.1268,
		695.86,-35.1845,743.069,-44.2244,776.215,-40.2067,820.41,-25.14,870.632,-22.1268,
		912.819,-37.1933,934.961,-44.1016,954.92,-38.8491,974.88,-45.152,996.94,-39.8996,
		1037.91,-49.354,1059.97,-63.0105,1086.23,-84.0204,1115.65,-85.0708,1153.46,-72.4649,
		1193.38,-77.7174,1227,-91.3739,1252.21,-110.283,1277.42,-108.182,1281.62,-127.091,
		1306.84,-141.103,1331,-144.255,1355.16,-155.81,1401.38,-175.77,1441.66,-173.359,
		1505.38,-153.709,1566.27,-143.124,1604.33,-141.244,1653.27,-154.234,1687.75,-141.272,
		1715.01,-112.084,1743.6,-57.5053,1764.91,-35.513,1783.52,-43.7838,1804.2,-96.8544,
		1805.57,-132.694,1812.47,-142.344,1873.12,-149.236,1931.01,-140.965,2023.37,-126.491,
		2066.1,-143.722,2088.85,-166.467,2102.63,-189.901,2130.89,-204.374,2168.8,-207.82,
		2234.96,-205.064,2261.58,-191.522,2286.18,-163.977,2316.62,-117.978,2340.88,-82.6962,
		2383.71,-55.007,2437.38,-41.609,2483.31,-37.5896,2507.01,-40.8224,2535.75,-46.8021,
		2553.06,-51.5425,2588.75,-80.737,2609.11,-94.0372,2626.65,-113.754,2646.7,-132.646,
		2671.69,-154.192,2686.72,-164.424,2714.27,-175.401,2758.47,-179.97,2952.34,-178.248,
		2952.34,-205.573,3064.31,-204.906,3099.63,-189.578,3177.37,-190.754,3184.57,-214.917,
		3274.26,-203.48,3324.75,-185.774,3421.53,-169.584,3484.18,-159.587,3559.48,-158.92,
		3613.47,-168.251,3646.79,-173.582,3681.45,-168.917,3695.73,-99.6964,3712.15,75.9802,
		3777.14,211.665,3829.27,115.972,3882.12,285.935,3908.54,203.096,3963.53,283.793,
		3989.95,55.2703,4049.94,183.814,4079.22,-4.00267,4092.79,-32.5679,4113.5,-41.1375,
		4167.77,-36.1386,4262.75,-9.0016,4372.57,29.6882,4504.22,43.4773,4649.69,49.6867,
		4674.48,29.5702,4713.99,14.847,4760.58,14.6627,4803.11,38.8688,4819.84,15.0291,
		4858.19,-1.45256,4896.91,5.9419,4925.06,31.9846,4960.49,17.0905,5006.14,15.8518,
		5050.86,24.3401,5078.48,41.8191,5498.61,41.7032,5499.5,-306.024, 5499.5, 300];
		this.explosion = null;
		this.bulletCount = 0;
		this.circle;
		this.hitOnce = false;
		this.facingRight=true;
		this.cursors;
		this.turret;
		this.bulletExists=false;
		this.bulletHitLand = false;
		this.redoTerrain = false;
		this.turWormDist;
		this.turWormLock;
		this.fireBut;
		this.worm;
		this.bullet;
		this.bulletEnabled = true;
		this.groundBody = null;
    };

    BasicGame.Game.prototype = {



        create: function () {
			this.game.world.setBounds(-10000, -10000, 20000, 20000);
	
			this.game.stage.backgroundColor = '#124184';

			// Enable Box2D physics
			this.game.physics.startSystem(Phaser.Physics.BOX2D);
			this.game.physics.startSystem(Phaser.Physics.ARCADE);
			this.game.physics.box2d.gravity.y = 500;
			this.game.physics.box2d.friction = 0.8;
			

			
			// Make the ground body
			this.groundBody = new Phaser.Physics.Box2D.Body(this.game, null, 0, 0, 0);
			//or change to setChain
/* 			var i = 0;
			for (i = 0; i < this.groundVertices.length; i ++){
				this.groundVertices[i] = 0;
			} */
			this.groundBody.setChain(this.groundVertices);
			this.groundBody.setCollisionCategory(2);
			var PTM = 50;

			
			var frequency = 3.5;
			var damping = 0.5;	
			
			this.cursors = this.game.input.keyboard.createCursorKeys();
			this.fireBut = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
			this.fireBut.onDown.add(this.fire, this);
			
			//var caption = this.game.add.text(5, 5, 'Simple car control. Left/right arrow keys to move, down arrow key to brake.', { fill: '#ffffff', font: '14pt Arial' });
			//caption.fixedToCamera = true;
			
			this.worm = this.add.sprite(100, -30, 'tank');
			this.game.physics.enable([this.worm], Phaser.Physics.BOX2D);
			this.worm.body.fixedRotation = true;
			this.createTurret();
			
			//turWormLock = this.game.physics.p2.createDistanceConstraint(crate1, player, 50);
			//turWormDist  = this.game.physics.p2.createLockConstraint(crate1, player, [0,-50], 0);
			
			this.game.camera.follow(this.worm);
			//groundBody.addCircle(30, 0, 0)
			
			

        },
		
		createTurret: function(){
			this.turret = this.game.add.sprite(this.worm.x + 12, this.worm.y + 5, 'turret');
			this.game.physics.enable(this.turret, Phaser.Physics.ARCADE);
		},

		fire: function (){
			if(!this.bulletExists){
			this.bulletExists = true;
			//if (bulletCount ===0){
				//bulletEnabeld = false;
				//bulletCount = 1;
				var p = new Phaser.Point(this.turret.x, this.turret.y);
				p.rotate(p.x, p.y, this.turret.rotation, false, 34);
				this.bullet = this.game.add.sprite(p.x, p.y, 'bullet');
				this.game.physics.box2d.enable(this.bullet);
				this.bullet.body.setCircle(5);
				//replace 600 with bullet-type speed
				p.setTo((Math.cos(this.turret.rotation) * 600), (Math.sin(this.turret.rotation) * 600));
				
				// Set velocity
				this.bullet.body.velocity.x = p.x;
				this.bullet.body.velocity.y = p.y;
				console.log("Fired");
				this.bullet.body.setCategoryContactCallback(2, this.worldCollideCallback, this);
				this.game.camera.follow(this.bullet)
			//} 
			//bulletEnabeld = false;
			//this.groundBody.setChain(this.groundVertices);
			/* if (this.redoTerrain){
			this.groundBody.setChain(this.groundVertices);
			this.ground.setCollisionCategory(2);
			} */
					this.hitOnce = false;
			}
			
		},

		worldCollideCallback: function (){
			this.bulletHitLand = true;
			
			
		},

		 update: function() {
			
			if (this.cursors.left.isDown && !this.cursors.right.isDown) { this.worm.body.moveRight(-100); this.turret.visible = false; this.turShown = false; this.facingRight = false;}
			else if (this.cursors.right.isDown && !this.cursors.left.isDown) { this.worm.body.moveRight(100); this.turret.visible = false; this.turShown = false; this.facingRight=true;}
			else if (this.cursors.up.isDown )
			{
				if(this.facingRight && this.turret.angle > -90){
					this.turret.angle--;
				}
				else if (!this.facingRight && this.turret.angle < 90){
					this.turret.angle++;
				}
			}
			else if (this.cursors.down.isDown )
			{
				if(this.facingRight && this.turret.angle < 0){
				this.turret.angle++;}
				else if (!this.facingRight && this.turret.angle < 90 &&this.turret.angle >0){
					this.turret.angle--;
				}
			}
			else { 
				if(this.worm.body.velocity.x === 0 && this.worm.body.velocity.y ===0){
				//createTurret();
				this.turret.visible = true;
				this.turret.x = this.worm.x;
				this.turret.y = this.worm.y;
				this.turret.anchor.setTo(0, 1);
				if(this.facingRight){this.turret.scale.x = 1;} else {this.turret.scale.x = -1;}
				}
			} // roll if no keys pressed
			
			if (this.bulletHitLand){
				//this.hitOnce = true;
				this.bullet.kill();
				this.bullet.destroy();
				this.bulletHitLand = false;	
				
				//console.log(this.bullet.x);
				//var explosion = new Phaser.Physics.Box2D.Body(this.game, null,0, 0, 0);
				//explosion.setCircle(50);
				var oldVertices = this.groundVertices;
				var p = new Phaser.Point(this.bullet.x, this.bullet.y);
				var minMax = [];
				var nullcount = 0;
				var maxIndex;
				var minStored = false;
				var maxStored = false;
				//this.bullet.destroy();
				this.explosion = new Phaser.Circle(p.x, p.y, 100);			
				var newCirc = this.createCircle(20, p, 50);
				
				//this.game.physics.box2d.createPolygon(p.x, p.y, newCirc);
				//console.log(this.explosion.x);
				//console.log(this.explosion.y);
				var i = 0;
				for (i = 0; i < this.groundVertices.length; i=i+2){
					
					p.x = this.groundVertices[i];
					p.y = this.groundVertices[i+1];
					//if (i == 0){console.log(p.x + " " + p.y);}
					if (this.explosion.contains(p.x, p.y)){
						console.log("point in circle");
						//console.log(i + " = " + this.groundVertices[i]);
						//console.log((i+1) + " = " + this.groundVertices[i+1]);
						//this.groundVertices[i] = null;
						//this.groundVertices[i+1] =null;
						var minIndex;
						nullcount ++;
						if(!minStored){
							minIndex = i;
							minMax.push(p.x);
							minMax.push(p.y);
							minStored = true;
						} else {
							minMax.push(p.x);
							minMax.push(p.y);
							maxIndex = i+1;
						}
						
					} //else {console.log("not in circle");}	
				}
/* 				minMax[2] = this.groundVertices[minIndex + ((nullcount*2)-2)];
				minMax[3] = this.groundVertices[minIndex + ((nullcount*2)-1)];
				console.log("NEW CIRC SIZE= " + newCirc.length);
				for (var poo = 0; poo < newCirc.length; poo=poo+2){
					console.log(newCirc[poo] + ", " + newCirc[poo+1] + "__");}
				
				if(nullcount == 1){
					minMax[2] = minMax[0];
					minMax[3] = minMax[1];
					//console.log("only 1 vertice in explosion");
				}
				if (minMax[0] > minMax[2]){
					var temp = minMax[0];
					minMax[0] = minMax[2];
					minMax[2] = temp;
				}
				if (minMax[1] > minMax[3]){
					var temp = minMax[1];
					minMax[1] = minMax[3];
					minMax[3] = temp;
				}
				minMax[0] = this.groundVertices.indexOf(minMax[0]-2);
				minMax[0] = this.groundVertices.indexOf(minMax[0]-1);
				//this.groundVertices.splice(14, 2);
				//for (i = 0; i < this.groundVertices.length; i++){
				//	if(this.groundVertices == null){
						//this.groundVertices(this.groundVertices.indexOf[minMa], 1);
					//}
				//}
				console.log("MIN VERTICE = " + minMax[0] + ", " + minMax[1]);
				console.log("MAX VERTICE = " + minMax[2] + ", " + minMax[3]);
				var spliceCount = 0;
				var toSplice = [];
				for (var l = 0; l < newCirc.length; l=l+2){
					console.log("hit splicer");
					if ((newCirc[l] >= minMax[0] && newCirc[l] <= minMax[2]) && (newCirc[l+1] >= minMax[3] ) ){
						toSplice[spliceCount] = newCirc[l];
						toSplice[spliceCount+1] = newCirc[l+1]
						spliceCount+=2;
						console.log("spliced");
					}
				}
				console.log("FROM MIN TO MAX, EXCLUDING SPLICED: ");
				for (l=minIndex; l<maxIndex; l+=2){
					console.log(this.groundVertices[l] + ", " + this.groundVertices[l+1]); 
				}
				var iter = 0;
				minIndex-=2;
				if(spliceCount == toSplice.length){console.log("ITS A MATCH");}
				for(l = spliceCount; l >0; l-=2){
					this.groundVertices.splice(minIndex+2+iter, 1, newCirc[l]);
					this.groundVertices.splice(minIndex+3+iter,1, newCirc[l+1]);
					iter+=2;
				}
				//minIndex-=6;
				console.log("___________");
				console.log("FROM MIN TO MAX, INCLUDING SPLICED: ");
				for (l=minIndex; l<maxIndex+(spliceCount/2); l+=2){
					console.log(this.groundVertices[l] + ", " + this.groundVertices[l+1]); 
				} */
				//console.log("FROM MIN TO MAX, INCLUDING SPLICED: " + this.groundVertices[minIndex] + ", " + this.groundVertices[minIndex+1] + " __s__ " + this.groundVertices[minIndex])
				/* for (i = 0; i < nullcount; i ++){
					var k = 0;
					for(k = 0; k < this.groundVertices.length; k++){
						if (this.groundVertices[i] === null){
							var j = 0;
							for (j = i; j < this.groundVertices.length; j++){
								this.groundVertices[j] = this.groundVertices[j + 1];
								//break;
							}
						
						}
						break;
					}
				} */
				/* for (i = 0; i < this.groundVertices.length; i++){
					if (this.groundVertices[i] === null){
						this.groundVertices.splice(i, nullcount);
					}
					break;
				} */
				
	/* 			for (i = 0; i < this.groundVertices.length; i++){
					this.groundVertices[i] = 0;
				} */
				//this.redoTerrain = true;
/* 				if(!this.hitOnce){
				this.groundBody.setChain(this.groundVertices);	
				this.groundBody.setCollisionCategory(2);}
				this.hitOnce = true; */
				//this.groundBody.setCategoryContactCallback(this.)
				var subj_paths = new ClipperLib.Paths();
				var subj_path = new ClipperLib.Path();
				for (var i = 0; i< this.groundVertices.length; i+=2){
				subj_path.push(
				new ClipperLib.IntPoint(this.groundVertices[i], this.groundVertices[i+1]));}
				subj_paths.push(subj_path);
				
				var clip_paths = new ClipperLib.Paths();
				var clip_path = new ClipperLib.Path();
				for(i=0; i < this.explosion.length; i+=2){
					clip_path.push(
					new ClipperLib.IntPoint(this.explosion[i], this.explosion[i+1]));
				}
				clip_paths.push(clip_path);
				
				var cpr = new ClipperLib.Clipper();
				
				var scale = 100;
				ClipperLib.JS.ScaleUpPaths(subj_paths, scale);
				ClipperLib.JS.ScaleUpPaths(clip_paths, scale);
				
				cpr.AddPaths(subj_paths, ClipperLib.PolyType.ptSubject, false);
				cpr.AddPaths(clip_paths, ClipperLib.PolyType.ptClip, true);
				var solution_paths = new ClipperLib.PolyTree();
				var clipType = ClipperLib.ClipType.ctIntersection;
				var subject_fillType = ClipperLib.PolyFillType.pftNonZero;
var clip_fillType = ClipperLib.PolyFillType.pftNonZero;
var succeeded = cpr.Execute(clipType, solution_paths, subject_fillType, clip_fillType);
				console.log("SOLUTION LENGTH: " + solution_paths[0]);
				if(!this.hitOnce){
				//this.groundBody.setChain(solution_paths);	
				this.groundBody.setCollisionCategory(2);}
				this.hitOnce = true;
				this.bulletExists=false;
				this.bulletExists=false;			
			}

		},

		createCircle: function (precision, origin, radius) {
			var angle=2*Math.PI/precision;
			var circleArray = [];
			var multiplier = 0;
			for (var i=0; i<(precision); i++) {
				//circleArray.push(new Phaser.Point(origin.x+radius*Math.cos(angle*i),origin.y+radius*Math.sin(angle*i)));
				multiplier = i*2;
				circleArray[multiplier] =(origin.x+radius*Math.cos(angle*i));
				circleArray[multiplier+1] = (origin.y+radius*Math.sin(angle*i));
			}
			//circleArray.splice(20, 20);
			return circleArray;
		},
		
		render: function () {

			this.game.debug.box2dWorld();
			//this.game.debug.geom(this.explosion,'rgba(0,255,0, 0.6)');

		},

        /**
         * Called by update if the bullet is in flight.
         *
         * @method bulletVsLand
         */
        bulletVsLand: function () {
			
            //  Simple bounds check
            if (this.bullet.x < 0 || this.bullet.x > this.game.world.width || this.bullet.y > this.game.height)
            {
                this.removeBullet();
                return;
            }

            var x = Math.floor(this.bullet.x);
            var y = Math.floor(this.bullet.y);
            var rgba = this.land.getPixel(x, y);
			console.log(rgba.a);
            if (rgba.a > 0)
            {
                this.land.blendDestinationOut();
                this.land.circle(x, y, 16, 'rgba(0, 0, 0, 255');
                this.land.blendReset();
                this.land.update();

                //  If you like you could combine the above 4 lines:
                this.land.blendDestinationOut().circle(x, y, 16, 'rgba(0, 0, 0, 255').blendReset().update();
				//THIS IS NEEDED TO SHOW TERRAIN-REMOVAL, BUT HORRIBLY INEFFICIENT - FIX IT FAM
				this.land.addToWorld();
				this.landsprite.destroy();
				this.landsprite = this.game.add.sprite(0,0,this.land);
				console.log("called bulletvsland");
                this.removeBullet();
            }

        },


    };

   // game.state.add('Game', BasicGame.Game, true);
