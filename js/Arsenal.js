Arsenal = function() {
		this._weapons = [];
		// 												id, 	sprite,	 			power, splash, shotsAllowed,   fuse, 	explosionRadius,   weaponType,	range, 	targetable,	bulletSpacing
		this._bazooka = 		new Projectile			(0, 	'bazooka',			1000, 	false, 		1, 			0,		125,				0,			null, 	false);
		this._normal = 			new Projectile			(1, 	'grenade', 			1000, 	false, 		1, 			2000, 	100,				0,			null,	false);
		this._dynamite =		new Projectile			(5, 	'dynamite', 		30, 	false, 		1, 			3000, 	100,				0,			null,	false);
		this._shotgun = 		new Ranged    			(2, 	'shotgun', 			500, 	false, 		2, 			0,		25,					1,			500,	false);
		this._baseballBat = 	new Melee	  	   		(3, 	'baseballBat', 		500, 	false, 		1,			0, 		25, 				2,			20,		false);
		this._teleporter = 		new TargettedSingle		(4, 	'teleporter', 		500, 	false, 		1,			0, 		105,				0,			null,	true);
		this._airstrike = 		new TargettedMulti 		(6, 	'airstrike', 		500, 	false, 		1, 			0, 		105,				0,			null,	true, 		40);
		this._clusterBomb = 	new ProjectileMulti		(7, 	'clusterGrenade', 	500, 	false, 		1, 			2000, 	65,					0,			null,	false);
		this._oldLady	 = 		new oldLady				(8, 	'oldLady', 			30, 	false, 		1, 			6000, 	200,				0,			null,	false);
		this._drill		 = 		new Drill				(9, 	'drill', 			30, 	false, 		1, 			0,	 	25,					0,			null,	false);
		this._homingMissile = 	new HomingProjectile	(10, 	'homingMissile',	1000, 	false, 		1, 			0,		125,				0,			null, 	true);
		this._superSheep	 = 	new Supersheep			(11, 	'supersheep',		1000, 	false, 		1, 			0,		125,				0,			null, 	false);
		this._sheep = 			new Sheep				(12, 	'sheep', 			30, 	false, 		1, 			5000, 	150,				0,			null,	false);
		this._girder	 = 		new Gurder				(13, 	'girder', 			500, 	false, 		1,			0, 		105,				0,			null,	true);
		this._skipTurn =		new Skip				(14, 	'oldLady', 			30, 	false, 		1, 			2500, 	200,				0,			null,	false);
		this._surrender =		new Surrender			(14, 	'oldLady', 			30, 	false, 		1, 			500, 	200,				0,			null,	false);
				
		
		this.addWeapon(this._bazooka);
		this.addWeapon(this._normal);
		this.addWeapon(this._shotgun);
		this.addWeapon(this._baseballBat);
		this.addWeapon(this._teleporter);
		this.addWeapon(this._dynamite);
		this.addWeapon(this._airstrike);
		this.addWeapon(this._clusterBomb);
		this.addWeapon(this._oldLady);
		this.addWeapon(this._drill);
		this.addWeapon(this._homingMissile);
		this.addWeapon(this._superSheep);
		this.addWeapon(this._sheep);
		this.addWeapon(this._girder);
		this.addWeapon(this._skipTurn);
		this.addWeapon(this._surrender);
		//this.addWeapon(this._mine);
};
	Arsenal.prototype = {
		getWeapons: function(){
			return this._weapons;
		},
		getWeapon: function(weaponIndex){
			return this._weapons[weaponIndex];
		},
		addWeapon: function(weapon){
			this._weapons.push(weapon);
		},
		changeWeapon: function(weaponIndex){
			return this._weapons[weaponIndex];
		},
	};