Gurder = function(id, sprite, power, splash, shotsAllowed, fuse, explosionRadius, weaponType,range, targetable){
		this._id = id;
		this._sprite = sprite;
		this._power = power;
		this.bulletExists = false;
		this.bullet;
		this._splash = splash;
		this._shotsAllowed = shotsAllowed;
		this._fuse = fuse;
		this._explosionRadius = explosionRadius;
		this._shotsInProgress = false;
		this._body;
		this._weaponType = weaponType;
		this._range = range;
		this._targetable = targetable;
		this.children;
		this._charging = false;
		this.canPlace = true;
	};
	
	Gurder.prototype = {
		getId: function(){
			return this._id;
		},
		
		showTargetting: function(context,game){
			context.targetting = undefined;
			context.targetting = game.add.sprite(context.input.x, context.input.y, this._sprite);
			
			context.game.physics.enable(context.targetting, Phaser.Physics.BOX2D);
			context.targetting.body.context = context;
			// ground body
			context.targetting.body.setCategoryContactCallback(2, function(a, b, c, d, begin, e){
				if (context.targetting != undefined){
					if (begin){
						console.log("begin: " + begin);
						this.canPlace = false;
						context.targetting.loadTexture('girder_bad');	
					} else {
						console.log("begin: " + begin);
						context.targetting.loadTexture('girder_good');
						this.canPlace = true;
					}
				}
			}, this);
			// worm bodies
			context.targetting.body.setCategoryPresolveCallback(3, function(a, b, c, d, begin, e){
				if (begin){
					this.canPlace = false;
				
					//b2contact objects contain an e_enabled flag of 0x0004, stored in the m_flags member. 
					//the below line is the implementation to DISABLE a contact object - the setEnabled function doesnt work with the phaser port GG
					begin.m_flags &= ~0x0004;
					context.targetting.loadTexture('girder_bad');
				} else {
					console.log("Changing it back");
					//b2contact objects contain an e_enabled flag of 0x0004, stored in the m_flags member.
					//the below line is the implementation to RE-ENABLE a contact object - the setEnabled function doesnt work with the phaser port GG
					begin.m_flags |= 0x0004;
					this.canPlace = true;
					context.targetting.loadTexture('girder_good');
					//contactObject.setEnabled(true);
				}
			}, this);
			//mine bodies
			context.targetting.body.setCategoryPresolveCallback(4, function(a, b, c, d, begin, e){
				if (begin){
					this.canPlace = false;
				
					//b2contact objects contain an e_enabled flag of 0x0004, stored in the m_flags member. 
					//the below line is the implementation to DISABLE a contact object - the setEnabled function doesnt work with the phaser port GG
					begin.m_flags &= ~0x0004;
					context.targetting.loadTexture('girder_bad');
				} else {
					console.log("Changing it back");
					//b2contact objects contain an e_enabled flag of 0x0004, stored in the m_flags member.
					//the below line is the implementation to RE-ENABLE a contact object - the setEnabled function doesnt work with the phaser port GG
					begin.m_flags |= 0x0004;
					this.canPlace = true;
					context.targetting.loadTexture('girder_good');
					//contactObject.setEnabled(true);
				}
			}, this);
			//context.targetting.body.static = true;
			context.targetting.body.fixedRotation = true;
			context.targetting.body.gravityScale = 0;
			context.targetting.anchor.setTo(0.5,0.5);
			//context.targetting.body.setEnabled(false);
			game.input.keyboard.stop();
			game.camera.follow(context.targetting);
		},
	
		
		getCharging(){
			return this._charging;
		},
		
		setCharging(charging){
			this._charging = charging;
		},
		
		getTargetable(){
			return this._targetable;
		},
		
		getRange: function(){
			return this._range;
		},
		getChildren(){
			return this.children;
		},
		getWeaponType: function(){
			return this._weaponType;
		},
		getSprite: function(){
			return this._sprite;
		},
		getPower: function(){
			return this._power;
		},
		draw: function(game, x, y){
			//var weapon = game.add.sprite(x,y, this._sprite);
			//console.log(weapon.width);
			this._body = game.add.sprite(x,y, this._sprite);
			return this._body;
		},
		getWidth: function(){
			return this._body.width;
		},
		getBody: function(){
			return this._body;
		},
		getShotsAllowed: function(){
			return this._shotsAllowed;
		},
		
		getFuse: function(){
			return this._fuse;
		},
		
		getExplosionRaidus: function(){
			return this._explosionRadius;
		},

		getShotsInProgress: function(){
			return this._shotsInProgress;
		},
		
		setShotsInProgress: function(shotsInProgress){
			this._shotsInProgress = shotsInProgress;
		},
		
		fire: function(args){
			if (!this.bulletExists){
				var x = args[0], y = args[1], game = args[2], context = args[3];
				var girder = game.add.sprite(x, y, this._sprite);
				var girderBounds = girder.getBounds();
				var landBounds = context.land.getBounds();
				//console.log("bounds: " + girderBounds);
				//console.log("bounds: " + landBounds);
				console.log("INTERSECTS: " + Phaser.Rectangle.intersects(girderBounds, landBounds));
				console.log("CAN PLACE AT TIME OF PLACEMENT: " + this.canPlace);
				if (this.canPlace){
					//console.log("CAN PLACE HERE");
					girder.anchor.setTo(0.5,0.5);
					/* context.game.physics.enable(girder, Phaser.Physics.BOX2D);
					girder.body.setCollisionCategory(2);
					girder.body.static = true;
					girder.body.gravityScale = 0; */
					//context.currBods.push(girder.body);
					var girderCorners =[];
					girderCorners.push(girder.x - (girder.width/2));
					girderCorners.push(girder.y - (girder.height/2));
					girderCorners.push(girder.x + (girder.width/2));
					girderCorners.push(girder.y - (girder.height/2));
					girderCorners.push(girder.x + (girder.width/2));
					girderCorners.push(girder.y + (girder.height/2));
					girderCorners.push(girder.x - (girder.width/2));
					girderCorners.push(girder.y + (girder.height/2));
					girderCorners.push(girder.x - (girder.width/2));
					girderCorners.push(girder.y - (girder.height/2));
					context.land.copy(girder);
					girder.destroy();
					
					context.currArrs.push(girderCorners);
					context.currBods.splice(0, context.currBods.length);
					context.currBods.push(context.groundBody.addChain(girderCorners));
					context.groundBody.setCollisionCategory(2);
					context.exploded = true;
					//context.fuseTimer.destroy();
					//add delay
					game.camera.follow(context.activeWorm.body);
					this.bulletExists = true;
				} else {
					girder.destroy();
				}
			} this.bulletExists = false;
			this.canPlace =true;
			//girder.body.fixedRotation = true;
		},
		
		explodeBullet: function(context,game){
			console.log("EXPLODING BULLET FOR PROJECTILE_MULTI");
			if (this.bulletExists){
				context.timerInEffect = true;
			this.bullet.kill();
			this.bullet.destroy();
			context.bulletHitLand = false;	
			var oldVertices = context.groundVertices;
			var p = new Phaser.Point(this.bullet.x, this.bullet.y);

			context.explosion = new Phaser.Circle(p.x,p.y, this.getExplosionRaidus());			
			var newCirc = context.createCircle(20, p, this.getExplosionRaidus()/2);
			var subj_paths = new ClipperLib.Paths();
			
			for(var x = 0; x < context.currArrs.length; x++){
				var subj_path = new ClipperLib.Path();
				var toPush = context.currArrs[x];
				for (var i = 0; i< toPush.length; i+=2){
					subj_path.push(
						new ClipperLib.IntPoint(toPush[i], toPush[i+1]));
				}		
				subj_paths.push(subj_path);
				subj_path = null;
			}

			var clip_paths = new ClipperLib.Paths();
			var clip_path = new ClipperLib.Path();
			for(i=0; i < newCirc.length; i+=2){
				clip_path.push(
					new ClipperLib.IntPoint(Math.round(newCirc[i]), Math.round(newCirc[i+1])));
			}
			clip_paths.push(clip_path);
			var cpr = new ClipperLib.Clipper();
			
			var scale = 1;
			ClipperLib.JS.ScaleUpPaths(subj_paths, scale);
			ClipperLib.JS.ScaleUpPaths(clip_paths, scale);
			
			cpr.AddPaths(subj_paths, ClipperLib.PolyType.ptSubject, true);
			cpr.AddPaths(clip_paths, ClipperLib.PolyType.ptClip, true);
			var solution_paths =[];
			var clipType = ClipperLib.ClipType.ctDifference;
			var subject_fillType = ClipperLib.PolyFillType.pftNonZero;
			var clip_fillType = ClipperLib.PolyFillType.pftNonZero;
			var succeeded = cpr.Execute(clipType, solution_paths, subject_fillType, clip_fillType);
		
			var newBods = [];
			var newArrs = [];
			var toX, toY;
			context.groundBody.clearFixtures();
			if(!context.hitOnce){
				if(this.getWeaponType() <= 1){
					for (i = 0; i < solution_paths.length; i++){
						var newTerrain = [];
						var poly = solution_paths[i];
						for(var j = 0; j < poly.length; j++){
							var vert = poly[j];
							if (j == 0){
								toX = vert.X; 
								toY = vert.Y;
							}
							newTerrain.push(vert.X);
							newTerrain.push(vert.Y);	
						}
						newTerrain.push(toX);
						newTerrain.push(toY);
						
						newBods.push(context.groundBody.addChain(newTerrain));	
						newArrs.push(newTerrain);
					}
					
					for (i = 0; i < context.currBods.length; i++){
						context.groundBody.removeFixture(context.currBods[i]);
					}
					context.currBods.splice(0, context.currBods.length);
					context.currArrs.splice(0, context.currArrs.length);
					for (i = 0; i < newBods.length; i++){
						context.currBods.push(newBods[i]);
					} 
					for (i = 0; i < newArrs.length; i++){
						context.currArrs.push(newArrs[i]);
					}	
					context.groundBody.setCollisionCategory(2);
					
					var x = Math.floor(this.bullet.x);
					var y = Math.floor(this.bullet.y);
					var rgba = context.land.getPixel(x, y);
					
					context.land.blendDestinationOut();
					context.land.circle(x, y, this.getExplosionRaidus()/2, 'rgba(0, 0, 0, 255');
					context.land.blendReset();
					context.land.update();
					context.land.addToWorld();			
					//MAX DX = 600 -> -600; MAX DY  = 700 -> -700;
				}
				this.effectWorms(0.5, game, context);
				context.hitOnce = true;
			}			
			this.bulletExists=false;
			context.exploded = true;
			//context.fuseTimer.destroy();
			//add delay
			game.camera.follow(context.activeWorm.body);			
			context.weaponPanel.bringToFront();
			this.children = context.arsenal.getWeapon(6);
				var args = [context.explosion.x, context.explosion.y, game, context, 15];
				this.children.fire(args);
				context.selectedWeapon = context.arsenal.getWeapon(6);
				//context.selectedWeapon.setBullets(this.children.getBullets());
			}
		},
		
		pathBullet: function(){
			
		},
		
		checkBulletLocation(){
			return true;
		},
		
		effectWorms: function(multiplier, game, context){
			for (var i = 0; i < context.turnSystem.getTeams().length; i++){
				for (var j = 0; j < context.turnSystem.getTeams()[i].getWholeTeam().length; j++){
					if (context.turnSystem.getTeams()[i].getWholeTeam()[j].isAlive()){
					//console.log("WIDTH: " + context.turnSystem.getTeams()[i].getWholeTeam()[j].getHeight());
					var topLeftY = context.turnSystem.getTeams()[i].getWholeTeam()[j].getY() - (context.turnSystem.getTeams()[i].getWholeTeam()[j].getHeight()/2);
					var topLeftX = context.turnSystem.getTeams()[i].getWholeTeam()[j].getX()-(context.turnSystem.getTeams()[i].getWholeTeam()[j].getWidth()/2);
					var width = context.turnSystem.getTeams()[i].getWholeTeam()[j].getWidth();
					var height = context.turnSystem.getTeams()[i].getWholeTeam()[j].getHeight();
					var lines = [
						new Phaser.Line(topLeftX, topLeftY, topLeftX + width, topLeftY),
						new Phaser.Line(topLeftX, topLeftY,topLeftX, topLeftY+height),
						new Phaser.Line(topLeftX + width, topLeftY, topLeftX+width, topLeftY+height),
						new Phaser.Line(topLeftX, topLeftY + height, topLeftX+width, topLeftY+height)
					];
					var intersects;
					//console.log("explosion: " + context.explosion.x + ", "+ context.explosion.y + ".... radius: " + context.explosion.radius);
					for(var p = 0; p < lines.length; p++) {
						var ar = lines[p].coordinatesOnLine();
						game.debug.geom(lines[p],'rgba(255,255,0, 1)');
						//console.log(p+ ":");
						for (var x = 0; x < ar.length; x++){
							//console.log(ar[x][0] + ", " + ar[x][1]);
							intersects = context.explosion.contains(ar[x][0], ar[x][1]);
							if (intersects) {
								break;
							}
						}
						if (intersects) {
							break;
						}
					}
					if (intersects){
						var dx =  context.turnSystem.getTeams()[i].getWholeTeam()[j].getX()-context.explosion.x ;
						var dy = context.turnSystem.getTeams()[i].getWholeTeam()[j].getY()-context.explosion.y  ;
						//console.log(dx + ", " + dy);
						dx*=30;dy*=30;	
						context.firePower = 0;
						
			//TEMP - TODO: APPLY CALCULATED DAMAGE, NOT 50 **********************************************			
						context.turnSystem.getTeams()[i].getWholeTeam()[j].applyDamage(50*multiplier);							
						var dist = context.explosion.distance(new Phaser.Point(context.turnSystem.getTeams()[i].getWholeTeam()[j].getX()-10,context.turnSystem.getTeams()[i].getWholeTeam()[j].getY()-10));
						var distDed = (dist/context.explosion.radius); console.log("distDed: " + distDed);
						//dx-=(dx*distDed);
						//dy-=(dy*distDed);
						
			//TEMP***************************************************************************************
						context.turnSystem.getTeams()[i].getWholeTeam()[j].body.body.velocity.x = dx-(dx*distDed);
						context.turnSystem.getTeams()[i].getWholeTeam()[j].body.body.velocity.y= dy-(dy*distDed);
			//TEMP***************************************************************************************
					}
				}
				}
			}
		},
};