Worm = function(x, y, game, context, sprite, colour, team){
		this._facingRight = true;
		this._hp = 100;
		this._alive = true;
		this._canJump = true;
		this._x = x;
		this._y = y;
		this.body = this.draw(game, sprite);
		this.teamColour =null;
		context.game.physics.enable([this.body], Phaser.Physics.BOX2D);
		this.body.body.collisionType = "wormType";
		this.body.body.fixedRotation = true;
		this.body.body.x = this._x ;
		this.body.body.y= this._y ;
		this.body.body.setCollisionCategory(3);
		//this.body.body.setCategoryContactCallback(2, context.resetcanJump, context);
		//console.log("initialized");
		this._hpLabel = game.add.text(this._x, this._y , this._hp, { fill: colour, font: '8pt Arial',stroke: 'rgba(255,255,255,1)'});
		//this._hpLabel.setShadow(0,0, 'rgab(255,255,255,1)', false);
		this._hpLabel.anchor.setTo(0.5,0.5);
		this._shotsTaken = 0;
		this.body.anchor.setTo(0.5,0.5);
		this._belowSensor = this.body.body.addCircle(8, 0, 10);
		this._lastWeapon = 0;
		this.body.body.setFixtureContactCallback(this._belowSensor, function(){this.setcanJumpTrue();}, this);
		this._team = team;
		this._colour = colour;
		this._belowSensor.SetSensor(true);
		console.log(this.getcanJump());
		this._walkSound = game.add.audio('wormWalk');
		this._walkSound2 = game.add.audio('wormWalk2');
		this.moveSpeed = 0;
	};
	Worm.prototype = {
		getFacingRight: function(){
			return this._facingRight;
		},
		
		getTeamColour: function(){
			return this._colour;
		},
		
		getHp: function(){
			return this._hp;
		},
		getX: function(){
			this._x = this.body.body.x;
			return this._x;
		},
		getY: function(){
			this._y = this.body.body.y;
			return this._y;
		},
		
		getTeamId: function(){
			return this._team;
		},
		
		getWidth: function(){
			return this.body.width;
		},
		getHeight: function(){
			return this.body.height;
		},
		getVelocityX: function(){
			return this.body.body.velocity.x
		},
		getVelocityY: function(){
			return this.body.body.velocity.y;
		},
		
		getHpLabel: function(){
			return this._hpLabel;
		},
		
		getcanJump: function(){
			return this._canJump;
		},
		isAlive: function(){
			return this._alive;
		},
		
		setFacingRight: function(facingRight){
			this._facingRight = facingRight;
		},
		setHp: function(hp){
			this._hp = hp
		},
		resetcanJump: function(){
			this._canJump = false;
			//console.log("false");
		},
		
		resetShotsTaken: function(){
			this._shotsTaken = 0;
		},
		
		getShotsTaken: function(){
			return this._shotsTaken;
		},
		
		setShotsTaken: function(shots){
			this._shotsTaken = shots;
		},
		
		setcanJumpTrue: function(){
			this._canJump = true;
		},
		
		setcanJump: function(canJump){
			if (canJump){
				this._canJump = true;
				this.jump();
			} else {
				this._canJump = false;
			}
		},
		getLastWeapon: function(){
			return this._lastWeapon;
		},
		setLastWeapon: function(index){
			this._lastWeapon = index;
		},
		
		outOfBounds: function(){
			if ((this.body.body.x > ($(window).width()*2) || this.body.body.x < ($(window).width()*-2)) || this.body.body.y >($(window).height()*2)){
				this.kill();
				return true;
			}
			return false;
		},
		
		removeHp: function(){
			this._hpLabel.destroy();
			//this.body.destroy();
			//delete this;
			//this = undefined;
		},
		
		jump: function(){
			this.body.body.velocity.y =-150;
			/*if (this._facingRight){
				this.body.body.velocity.x = 75;
			} else {this.body.body.velocity.x = -75;}*/
			this.body.body.velocity.x = 1.5*this.moveSpeed;
			//this._canJump = true;
			this._canJump = false;
		},
		
		isStationary: function(){
			return (this.body.body.velocity.y == 0 && this.body.body.velocity.x == 0);
		},
		
		kill: function(){
			this._alive = false;
			this.removeHp();
			var num = Math.floor((Math.random() * 6) + 1); 
			this.body.loadTexture('tombstone' + num);
			//TODO remove from world, play kill animation
		},
		
		applyDamage: function(damage){
			var worm =this;
			setTimeout(function(){
				if(damage >= worm._hp){
					worm.kill();
				}
				var finalHp = worm._hp - damage;
				while (worm._hp > finalHp){
					worm._hp--;
					worm._hpLabel.setText(worm._hp);
				}
			},0);
		},
		
		draw: function(game, sprite){
			
			return game.add.sprite(this._x, this._y, sprite);
		},
		
		move: function(speed){
			this.body.body.moveRight(speed);
			this.moveSpeed = speed;
			if (this.body.body.velocity.x != 0){
			this._walkSound.play("", 0, 1, false, false);}
				//this._walkSound2.play("", 0, 1, false, false);
		},
};