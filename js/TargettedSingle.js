TargettedSingle = function(id, sprite, power, splash, shotsAllowed, fuse, explosionRadius, weaponType,range, targetable){
		this._id = id;
		this._sprite = sprite;
		this._power = power;
		this.bulletExists = false;
		this.bullet;
		this._splash = splash;
		this._shotsAllowed = shotsAllowed;
		this._fuse = fuse;
		this._explosionRadius = explosionRadius;
		this._shotsInProgress = false;
		this._body;
		this._weaponType = weaponType;
		this._range = range;
		this._targetable = targetable;
		this.bulletExists = false;
	};
	
	TargettedSingle.prototype = {
		getId: function(){
			return this._id;
		},
		
		getTargetable(){
			return this._targetable;
		},
		
		setCharging(charging){
			this._charging = charging;
		},
		
		getCharging(){
			return this._charging;
		},
		
		getRange: function(){
			return this._range;
		},
		getWeaponType: function(){
			return this._weaponType;
		},
		getSprite: function(){
			return this._sprite;
		},
		getPower: function(){
			return this._power;
		},
		
		pathBullet: function(){
			
		},
		
		draw: function(game, x, y){
			//var weapon = game.add.sprite(x,y, this._sprite);
			//console.log(weapon.width);
			this._body = game.add.sprite(x,y, this._sprite);
			return this._body;
		},
		getWidth: function(){
			return this._body.width;
		},
		getBody: function(){
			return this._body;
		},
		getShotsAllowed: function(){
			return this._shotsAllowed;
		},
		
		getFuse: function(){
			return this._fuse;
		},
		
		getExplosionRaidus: function(){
			return this._explosionRadius;
		},

		getShotsInProgress: function(){
			return this._shotsInProgress;
		},
		
		setShotsInProgress: function(shotsInProgress){
			this._shotsInProgress = shotsInProgress;
		},
		
		showTargetting: function(context,game){
			context.targetting = undefined;
			context.targetting = game.add.sprite(context.input.x, context.input.y, 'targetCrosshair');
			context.targetting.anchor.setTo(0.5,0.5);
			game.input.keyboard.stop();
			game.camera.follow(context.targetting);
		},
		
		fire: function(args){
			if (!this.bulletExists){
				args[3].timerInEffect = true;
				var worm = args[0], x = args[1], y = args[2];
				worm.body.body.x = x;
				worm.body.body.y = y;
				console.log("moving worm FROM WEAPON FIRE");
				//this is needed to fix bug where worm doesnt fall after being teleported - no idea why 
				worm.move(0.1);
				this.bulletExists = true;
			}
		},
		checkBulletLocation(){
			if (this.bulletExists){
				this.bulletExists = false;
				return true;
			}
		},
		
	};