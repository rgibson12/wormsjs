
BasicGame.Game = function (game) {

		this.groundVertices = [37,200,39,208,39,223,36,234,35,238,31,243,30,248,36,261,30,269,29,280,30,296,23,300,18,307,18,340,
			6,360,5,392,19,429,20,453,21,463,31,500,16,522,13,545,23,578,350,578,352,519,359,494,355,483,349,475,
			345,474,336,473,336,460,351,411,350,400,347,393,342,388,339,379,338,363,322,333,302,305,274,258,266,255,
			252,252,248,238,246,225,237,209,229,179,221,179,220,159,217,146,202,130,191,125,180,124,179,130,174,134,
			171,142,141,142,126,151,126,189,97,216,98,246,101,275,80,298,77,310,75,324,68,330,67,319,63,310,55,297,
			60,289,60,280,55,269,59,263,62,255,60,247,64,245,65,242,59,238,61,221,68,217,67,213,47,200,37,200];
		this.x=0; this.y=0; this.i=1; this.r=0;
		this.powerCol = 122;
		this.timeStamp = 0;
		this.explosion = null;
		this.hitOnce = false;
		this.allWormsHarmed = false;
		this.opacity =0 ;
		this.timerInEffect = true;
		this.turretPointer;
		this.tFacingRight = true;
		this.cursors;
		this.backupWorm;
		this.jumpButton;
		this.damagedWormsOnce = false;
		this.turret;
		this.targetting;
		//this.selectedWeapon.bulletExists=false;
		this.bulletHitLand = false;
		this.fuseTimer;
		this.fireBut;
		this.activeWorm;
		this.changeTimer = 0;
		this.weaponUsed = false;
		this.powerIndicator = [];
		this.fireBallR;
		this.currBods = [];
		this.currArrs = [];
		//this.selectedWeapon.bullet;
		this.arsenal = [];
		this.groundBody = null;
		this.selectedWeapon;
		this.land;
		this.weaponChange;
		this.weaponPanel;
		this.weaponButton;
		this.weaponPanelTimer=0;
		this.debugOn = false;
		this.debugOnBut;
		this.firePower = 0;
		this.fireDown = false;
		this.turnSystem;
		this.crossHair;
		this.exploded = false;
		this.firedPower;
		this.mines = [];
		this.cTimer;
		this.cTimerEvent;
		
		this.wormsDamaged = [];
    };

    BasicGame.Game.prototype = {
		createArsenal: function(){
			this.arsenal = new Arsenal();
			this.selectedWeapon = this.arsenal.getWeapon(0);
			this.firePower = 0;	
		},
		
		explodeMine: function(mineBody, contactingBody){
			//mine = this.mines[i];
			//mine.explodeBullet(this, this.game);
			if (contactingBody.collisionType == "wormType"){
				console.log(this.mines.length);
			}
		},
		
		createLand: function(){
			this.land = this.add.bitmapData(1500, 500);
            this.land.draw('background');
            this.land.update();
            this.land.addToWorld();
		},

        create: function () {
			this.createLand();
			this.createArsenal();
			this.game.world.setBounds(0, 0, 1000, 1000);
			this.currArrs.push(this.groundVertices);
			
			this.cTimer = this.game.time.create();
        
			// Create a delayed event 1m and 30s from now
			this.cTimerEvent = this.cTimer.add(Phaser.Timer.SECOND * 60, this.endTimer, this);
        
			// Start the t
			this.cTimer.start();
			
			this.game.stage.backgroundColor = '#124184';
			// Enable Box2D physics
			this.game.physics.startSystem(Phaser.Physics.BOX2D);
			this.game.physics.startSystem(Phaser.Physics.ARCADE);
			this.game.physics.box2d.gravity.y = 500;
			this.game.physics.box2d.friction = 0.8;
			for (var i = 0; i < this.groundVertices.length; i++){			
				this.groundVertices[i] = Math.round(this.groundVertices[i]);
			}
			
			// Make the ground body
			this.groundBody = new Phaser.Physics.Box2D.Body(this.game, null, 0, 0, 0);
			this.currBods.push(this.groundBody.setPolygon(this.groundVertices));
			this.groundBody.setCollisionCategory(2);	

			this.cursors = this.game.input.keyboard.createCursorKeys();
			this.fireBut = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
			this.jumpButton = this.game.input.keyboard.addKey(Phaser.Keyboard.J);
			this.weaponChange = this.game.input.keyboard.addKey(Phaser.Keyboard.C);
			this.weaponButton = this.game.input.keyboard.addKey(Phaser.Keyboard.W);
			this.debugOnBut = this.game.input.keyboard.addKey(Phaser.Keyboard.R);
			
			
			this.fireBut.onDown.add(this.charge, this);
			
			this.fireBut.onUp.add(this.fire, this);
			this.game.input.onDown.add(this.processClick,this);
			this.showWeaponPanel();
			
			this.debugOnBut.onDown.add(this.debug, this);
			this.turnSystem = new Turnsystem(this.game, this);
			this.activeWorm = this.turnSystem.getStartingWorm();
			this.createTurret();			
			this.turret.visible = false;
			
			
			
			this.crosshair.visible = false;
			this.game.camera.follow(this.activeWorm.body);
			this.timeStamp = this.game.time.now;
			this.mines.push(new Mine(0, 'mine', 100, 150,150, this.game, this));
			

        },
		
		debug: function(){
			this.debugOn = !this.debugOn;
			this.render();
		},
		
		createTurret: function(){
			this.turret = this.selectedWeapon.draw(this.game, this.activeWorm.getX(),  this.activeWorm.getY());
			this.turret.rotation = 0;
			this.activeWorm.setFacingRight(true);		
			this.turretPointer = new Phaser.Line(this.turret.x+this.selectedWeapon.getWidth(), this.turret.y-5, this.turret.x+70, this.turret.y);
			this.crosshair = this.game.add.sprite(this.turret.x+90,this.turret.y,'crosshair');
		},

		resetcanJump: function(){
			this.activeWorm.resetcanJump();
		},
		
		charge: function(){
			if (this.activeWorm.getShotsTaken() < this.selectedWeapon.getShotsAllowed()){
				if(this.selectedWeapon.getWeaponType() >= 1){
					this.firePower = this.selectedWeapon.getPower();
					console.log("not charging");
				} else {
					this.selectedWeapon.setCharging(true);
					this.fireDown = true;
					this.fireBallR = 10;
					this.powerIndicator = [];
				}
			}
		},
		
		resetCharge: function(){
			this.selectedWeapon.setCharging(false);
					this.fireDown = false;
					this.fireBallR = 0;
					this.firePower = 0;
					this.powerIndicator = [];
					console.log("resetting charge");
		},
		
		fire: function (xT,yT){
			if (this.selectedWeapon.getCharging()){
				if (this.activeWorm.getShotsTaken() < this.selectedWeapon.getShotsAllowed()){
					this.cTimer.pause();
					this.timerInEffect = false;
					this.activeWorm.setShotsTaken(this.activeWorm.getShotsTaken() + 1)
					this.powerIndicator.splice(0, this.powerIndicator.length);
					this.fireDown = false;
					this.selectedWeapon.setShotsInProgress(true);
					if (this.selectedWeapon.getShotsAllowed()>1){
						this.weaponPanel.disable();
					}
					this.opacity = 0;
					this.weaponUsed = true;
					this.selectedWeapon.fire(this, this.game);
					//if weapon has a fuse, start it ticking from the moment its fired
					if(this.selectedWeapon.getFuse() > 0){
						this.fuseTimer = this.game.time.create(this.game, true);
						this.fuseTimer.add(this.selectedWeapon.getFuse(), this.doExplosion, this);
						this.fuseTimer.start();
					}
				
				}
			}
		},
 
		worldCollideCallback: function() {
			if (this.selectedWeapon.getFuse() == 0){
				this.fuseTimer = this.game.time.create(this.game, true);
				this.fuseTimer.add(this.selectedWeapon.getFuse(), this.doExplosion, this);
				this.fuseTimer.start();
			}
			
		},

		updateMines: function(){
			for (var i = 0; i < this.mines.length; i++){
				if (this.mines[i].getActive()){
					var sensor = new Phaser.Circle(this.mines[i].bullet.body.x, this.mines[i].bullet.body.y, 50);
					
					for (var p = 0; p < this.turnSystem.getTeams().length; p++){
						for (var j = 0; j < this.turnSystem.getTeams()[p].getWholeTeam().length; j++){
							if (this.turnSystem.getTeams()[p].getWholeTeam()[j].isAlive()){
								if(sensor.contains(this.turnSystem.getTeams()[p].getWholeTeam()[j].getX(), this.turnSystem.getTeams()[p].getWholeTeam()[j].getY())){
									this.mines[i]._active = false;
									this.mines[i].explodeBullet(this, this.game);
								}
							}
						}
					}
				}
			}
		},
		
		update: function() {
			if (this.selectedWeapon.bulletExists){
				this.selectedWeapon.pathBullet();
			}
			this.updateMines();
			
			if (this.selectedWeapon._bulletHoming != undefined && this.selectedWeapon._bulletHoming == true){
				this.selectedWeapon.homeBullet(this.game,this);
			}
	//***************************************************************************
			if (this.selectedWeapon instanceof oldLady || this.selectedWeapon instanceof Sheep || this.selectedWeapon instanceof Skip){
				if (this.fuseTimer != undefined){
					if (this.selectedWeapon.bullet != undefined){
						if (this.fuseTimer.elapsed < this.selectedWeapon.getFuse()){
							this.selectedWeapon.walk();
							
						}
					}
				}
			}
	//****************************************************************************
			if(this.turnSystem.checkWormPositions(this.activeWorm)){
				this.changeWorm();
			}
			if (this.weaponUsed){
				if (this.selectedWeapon.checkBulletLocation()){
					this.weaponUsed = false;
					//if (this.turnSystem.allWormsStationary()){
						this.changeWorm();
						
					//}
				}
			}
			if(this.targetting != undefined){
				if(this.selectedWeapon instanceof Gurder){
					this.targetting.body.x = this.input.x;
					this.targetting.body.y = this.input.y;
				} else {
					this.targetting.x = this.input.x;
					this.targetting.y = this.input.y;
				}
			}
			if (this.selectedWeapon.bulletExists && (this.selectedWeapon.getWeaponType()>=1)){
				if (Math.abs(this.selectedWeapon.bullet.x - this.turret.x) > this.selectedWeapon.getRange() || Math.abs(this.selectedWeapon.bullet.y- this.turret.y) > this.selectedWeapon.getRange()  ){
					this.doExplosion();
				}
			}
			for (var i = 0; i < this.turnSystem.getTeams().length; i++){
				this.turnSystem.getTeams()[i].alignHpLabels();
			}	
			if ((this.game.time.now - this.timeStamp >= 60000) || (this.turnSystem.allWormsStationary())&& this.exploded && this.activeWorm.getShotsTaken() >= this.selectedWeapon.getShotsAllowed()){
				
					if (this.timerInEffect){
						if (this.wormsDamaged.length > 0){
							if (!this.damagedWormsOnce){
								this.damagedWormsOnce = true;
								this.turnSystem.applyAllDamage(this, this.game);
							}
							if(this.allWormsHarmed){
								this.changeWorm();
								this.allWormsHamred = false;
							}
						} else{
							this.changeWorm();
						}
					}
				
			}
			if (this.fireDown && (this.activeWorm.getShotsTaken() < this.selectedWeapon.getShotsAllowed())){
				var opacityMulti = (this.selectedWeapon.getPower())/10;
				if (this.firePower < this.selectedWeapon.getPower() ){
					this.firePower+=10;
					this.opacity +=(1/opacityMulti);
					
					var ar = [];
					this.turretPointer.coordinatesOnLine(1, ar);
					if (this.powerIndicator[0] == undefined){
						this.powerIndicator.push(new Phaser.Circle(ar[0].x, ar[0].y, 5));
						this.x = this.turret.x;
						this.y = this.turret.y-25;
						this.r = 5;
						this.i=1;
					} else {
						this.r+=0.2;
						if(this.i < ar.length){
							this.powerIndicator.push(new Phaser.Circle(ar[this.i][0],ar[this.i][1], this.r));
						}
						this.i++;
					}
					this.powerCol++;
				}
			}
			if (this.cursors.left.isDown && !this.cursors.right.isDown) { 
				this.activeWorm.move(-50); 
				this.turret.visible = false; 
				this.crosshair.visible = false; 
				this.turShown = false; 
				this.activeWorm.setFacingRight(false);
				if (this.selectedWeapon.getCharging()){
					this.resetCharge();
				}
			}
			else if (this.cursors.right.isDown && !this.cursors.left.isDown) { 
				this.activeWorm.move(50); 
				
				this.turret.visible = false; 
				this.crosshair.visible = false;
				this.turShown = false; 
				this.activeWorm.setFacingRight(true);
				if (this.selectedWeapon.getCharging()){
					this.resetCharge();
				}
			}
			else if (this.cursors.up.isDown && this.activeWorm.body.body.velocity.x==0 &&  this.activeWorm.body.body.velocity.y==0){
				if(this.activeWorm.getFacingRight() ){
					this.turret.angle--;
				}
				else if (!this.activeWorm.getFacingRight() ){
					this.turret.angle++;
				}
				if (this.selectedWeapon.getCharging()){
					this.resetCharge();
				}
			}
			else if (this.cursors.down.isDown && this.activeWorm.body.body.velocity.x==0 &&  this.activeWorm.body.body.velocity.y==0) { 
				if(this.activeWorm.getFacingRight() ){
					this.turret.angle++;
				} else if (!this.activeWorm.getFacingRight()){
					this.turret.angle--;
				}
				if (this.selectedWeapon.getCharging()){
					

				}
			}
			if (this.jumpButton.isDown && this.activeWorm.getcanJump() && this.activeWorm.body.body.velocity.x==0 &&  this.activeWorm.body.body.velocity.y==0) {
				this.activeWorm.resetcanJump();
				//console.log("jump status: " +this.activeWorm.getcanJump());
				this.activeWorm.jump();
				//console.log("jump status: " +this.activeWorm.getcanJump());
				//console.log(this.activeWorm.getcanJump());	
				if (this.selectedWeapon.getCharging()){
					this.resetCharge();
				}
			}
			else { 
				if(this.activeWorm.body.body.velocity.x === 0 && this.activeWorm.body.body.velocity.y ===0 && this.activeWorm.getcanJump()){
					this.turret.visible = true;
					this.crosshair.visible = true;
					this.turret.x = this.activeWorm.getX();
					this.turret.y = this.activeWorm.getY();
					
					this.turret.anchor.setTo(0,0.5);
					this.crosshair.anchor.setTo(0.5,0.5);
					if(this.activeWorm.getFacingRight()){
						if(!this.tFacingRight){
							this.turret.scale.x = 1;
							this.turret.rotation = - this.turret.rotation;
							this.tFacingRight = true;
						}
					} else {
						if(this.tFacingRight){
							this.turret.scale.x = -1;
							this.turret.rotation = - this.turret.rotation;
							this.tFacingRight = false;
						}
					}
					if (!this.selectedWeapon.getTargetable() || this.selectedWeapon instanceof HomingProjectile){
						var p = new Phaser.Point(this.activeWorm.getX(),this.activeWorm.getY());
						p.rotate(p.x, p.y, this.turret.rotation,false,this.selectedWeapon.getWidth());
						if (this.activeWorm.getFacingRight()){
							this.turretPointer.fromAngle(p.x,p.y,this.turret.rotation, 70);
							p.rotate(p.x, p.y, this.turret.rotation,false,90)
						} else {
							this.turretPointer.fromAngle(p.x,p.y,this.turret.rotation, -70);
							p.rotate(p.x, p.y, this.turret.rotation,false,-90);
						}			
						this.crosshair.x = p.x;
						this.crosshair.y = p.y;
					}
				}
			} 
			if(this.weaponButton.isDown && this.weaponPanelTimer < this.game.time.now){
				if(this.game.paused){
					this.changeWeapon();
				}
				else {
					this.showWeaponPanel();					
				}
				this.weaponPanelTimer = this.game.time.now + 650;
			}		
		},
		
		endGame: function(){
			//this.game.paused = true;
			this.state.start('EndGame');
		},
		
		changeWorm: function(){
			
			this.game.input.mouse.enabled = true;

			this.activeWorm.resetShotsTaken();
			this.activeWorm.setLastWeapon(this.selectedWeapon.getId());
			this.activeWorm = this.turnSystem.progressTurn(this);
			
			if (this.activeWorm != false){
				this.selectedWeapon.setShotsInProgress(false);
				this.weaponPanel.enable();
				console.log("wormChanged");
				this.game.camera.follow(this.activeWorm.body);
				this.turret.destroy();
				this.selectedWeapon = this.arsenal.getWeapon(this.activeWorm.getLastWeapon());
				if (this.selectedWeapon.getTargetable()){
					this.selectedWeapon = this.arsenal.getWeapon(0);
				}
				this.crosshair.destroy();
				this.createTurret();
				this.timeStamp = this.game.time.now;
				if (this.targetting != undefined){
					this.game.input.keyboard.start();
					this.targetting.destroy();
					this.targetting = undefined;
				}
				
				this.cTimer.destroy();
						this.cTimer = this.game.time.create();
						this.cTimerEvent = this.cTimer.add(Phaser.Timer.SECOND * 60, this.endTimer, this);
						this.cTimer.start();
						
			} else {
				//********** START END GAME STATE ******************
				this.activeWorm = this.turnSystem.getTeams()[1].getWorm(0);
				this.endGame()
			}
		},
		
		doExplosion: function(){
			this.selectedWeapon.explodeBullet(this, this.game);
		},
		
		showWeaponPanel : function(){
			this.weaponPanel = new WeaponPanel($(window).width()/2,$(window).height()-15);
			this.weaponPanel.show(this.game);
		},
		
		changeWeapon: function(event, x1, y1){
			var x = event.x - x1, y = event.y - y1;
			//console.log("X IS: " + x + "...Y IS: " + y);
			if (y<28 && y >0){
				if (this.weaponPanel != undefined && !this.selectedWeapon.getShotsInProgress()){
					changeWeaponIndex = this.resolveClick(event, this.selectedWeapon.getId(), true,x,y);
					if (changeWeaponIndex != this.selectedWeapon.getId()){
						this.selectedWeapon = this.arsenal.changeWeapon(changeWeaponIndex);
						this.firePower = 0;
						this.turret.destroy();
						this.crosshair.destroy();
						this.createTurret();
						this.changeTimer+= 650;
						if (this.selectedWeapon.getTargetable()){
						console.log("adding target");
						this.selectedWeapon.showTargetting(this, this.game);
						//else OTHER TARGETTED WEAPONS	
						} else {
							if (this.targetting!= undefined){
								this.game.input.keyboard.start();
								this.targetting.destroy();
								this.targetting = undefined;
							}
						}
					}
				}
			}	
		},
		
		processClick: function(event){
			//area occupied by weapon bar
			var x1 = ($(window).width()/2) - 118;
			var x2 =($(window).width()/2) + 118;
			var y1 = ($(window).height()) - 31;
			var y2 = ($(window).height());
			
			var switchspaceLeft = x1 - 30, switchspaceRight = x2+30;
			if ((x1 < event.x && event.x <x2) && (y1 < event.y && event.y < y2)){
				this.changeWeapon(event, x1, y1);
				
			}	else if (this.targetting != undefined){
					var args = [];
					if (this.selectedWeapon.getId() == 4){
						args.push(this.activeWorm);
						args.push(this.input.x);
						args.push(this.input.y);	
						args.push(this);
						this.selectedWeapon.fire(args);
						this.weaponUsed = true;						
					} else if (this.selectedWeapon.getId() == 6){
						args.push(this.input.x);
						args.push(this.input.y);
						args.push(this.game);
						args.push(this);
						args.push(40);
						this.selectedWeapon.fire(args);
						this.weaponUsed = true;
						//console.log("not moving worm");
						//OTHER TARGETTED WEAPONS
					} else if (this.selectedWeapon.getId() == 10) {
						args.push(this.input.x);
						args.push(this.input.y);
						args.push(this.game);
						args.push(this);
						//args.push(40);
						this.selectedWeapon.setTarget(args);
					//this.weaponUsed = true
					} else {
						args.push(this.input.x);
						args.push(this.input.y);
						args.push(this.game);
						args.push(this);
						this.selectedWeapon.fire(args);
						console.log("LETS DO IT");
						this.weaponUsed = true;
					}
					
					//this.changeWorm();
			}	else if	((switchspaceLeft < event.x && event.x <x1) && (y1 < event.y && event.y < y2)){
					//console.log("SWITCHING WEAPON PANEL");
					this.weaponPanel.regress();
			}	else if	((x2 < event.x && event.x <switchspaceRight) && (y1 < event.y && event.y < y2)){
					//console.log("SWITCHING WEAPON PANEL");
					this.weaponPanel.progress();
			}   else {
					console.log("OTHER CLICK");
			}			
		},
		
		getGroundBody:function(){
			return this.groundBody;
		},
		getCurrArrs: function(){
			return this.currArrs;
		},
		getCurrBods: function(){
			return this.currBods;
		},
		getLand: function(){
			return this.land;
		},
		resolveClick: function(event, currIndex, weaponChange,x,y){
			//var x = event.x - x1, y = event.y - y1;
			var changeWeaponIndex = undefined;
			//console.log("X IS : " + x + " ...Y IS: " + y);
			//each weapon icon is 28x28 -> add 28 to the x for each check
			if (weaponChange){
				//width of weapon panel
				if (x<224){
					currIndex = this.weaponPanel.resolveClick(x, currIndex);
					console.log("currindex: " + currIndex);
					this.game.camera.follow(this.activeWorm.body);
					//teleporter or airstrike
					
				}
				return currIndex;
			} else {
				//console.log("OTHER");
			}
		
			
		},
		
		showTargetting: function(){
			this.targetting = undefined;
			this.targetting = this.game.add.sprite(this.input.x, this.input.y, 'targetCrosshair');
			this.targetting.anchor.setTo(0.5,0.5);
			this.game.input.keyboard.stop();
			this.game.camera.follow(this.targetting);
		},

		createCircle: function (precision, origin, radius) {
			var angle=2*Math.PI/precision;
			var circleArray = [];
			var multiplier = 0;
			for (var i=0; i<(precision); i++) {
				multiplier = i*2;
				circleArray[multiplier] =(origin.x+radius*Math.cos(angle*i));
				circleArray[multiplier+1] = (origin.y+radius*Math.sin(angle*i));
			}
			return circleArray;
		},
		
		endTimer: function() {
			// Stop the timer when the delayed event triggers
			this.cTimer.stop();
		},
		
		formatTime: function(s) {
			// Convert seconds (s) to a nicely formatted and padded time string
			var minutes = 0
			var seconds = "0" + (s - minutes * 60);
			return  seconds.substr(-2);   
		},
		
		render: function () {
			if(this.debugOn){
				this.game.debug.text(this.game.time.fps || '--', 2, 14, "#00ff00");
				this.game.debug.box2dWorld();
				this.game.debug.geom(this.explosion,'rgba(0,255,0, 0.6)');
				this.game.debug.geom(this.turretPointer,'rgba(0,255,0, 0.6)');
			}
			
			this.game.debug.text(this.formatTime(Math.round((this.cTimerEvent.delay - this.cTimer.ms) / 1000)), $(window).width()-30, 21, this.activeWorm.getTeamColour());
			
			
			//this.game.debug.geom(this.point,'rgb222,18,18, 1)');
			for (i=0; i < this.powerIndicator.length; i++){
			this.game.debug.geom(this.powerIndicator[i],'rgba(' + this.powerCol + ',18,18,1)');}

		},
    };