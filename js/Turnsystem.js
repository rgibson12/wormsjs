Turnsystem = function(game, context){
		//Teams
		this._team1 = new Team(game, context, "tank", 2, '#0000FF', 1);
		this._team2 = new Team(game, context, "tank", 2, '#FF0000', 2);
		
		//Reference to Teams
		this._teams = [];
		this._teams.push(this._team1);
		this._teams.push(this._team2);
		
		//Turn counter
		this._currentTurn = 0;
		
		//Team alternator
		this._team1Go = false;
		this._firstGoStopper = true;	
		
		this._currTeam1Index = 1;
		this._currTeam2Index = 0;
	};
	Turnsystem.prototype = {
		getTeams: function(){
			return this._teams;
		},

		getStartingWorm: function(){
			return this._team1.getWorm(0);
		},
		
		checkWormPositions: function(activeWorm){
			var team = this._team1.getWholeTeam(), team2 = this._team2.getWholeTeam();
			
			for (var i = 0; i<team.length; i++){
				if (team[i].outOfBounds() && activeWorm == team[i]){
					return true;
				}
			}
			for (var i = 0; i<team2.length; i++){
				if (team2[i].outOfBounds() && activeWorm == team2[i]){
					return true;
				}
			}
			return false;
		},

		isWholeTeamDead: function(teamIndex){
			var team = this._teams[teamIndex].getWholeTeam();
			//console.log("team size: " + team.length);
			for (var i = 0; i < team.length; i++){
				//console.log("Checking worm " + i + " on team "+teamIndex)
				if (team[i].isAlive()){
					//console.log("Still worms on both teams");
					return false
				}
			}
			//console.log("whole team is dead");
			return true;
		},
		
		allWormsStationary: function(){
			var team =this._team1.getWholeTeam();
			for (var i = 0; i < team.length; i++){
				//console.log("Checking worm " + i + " on team "+teamIndex)
				if (team[i].isAlive()){
					if (!team[i].isStationary()){
						//console.log("Still worms on both teams");
						return false
					}
				}
			}
			
			team =this._team2.getWholeTeam();
			for (var i = 0; i < team.length; i++){
				//console.log("Checking worm " + i + " on team "+teamIndex)
				if (team[i].isAlive()){
					if (!team[i].isStationary()){
						//console.log("Still worms on both teams");
						return false
					}
				}
			}
			return true;
		},
		
		applyAllDamage: function(context, game){
			console.log("WORMS TO HARM: "+context.wormsDamaged.length);
			for (var i = 0; i <context.wormsDamaged.length; i++){
				(function(i){
					setTimeout(function(){
						console.log("harming worm x: " + i);
						var worm = context.wormsDamaged[i];
						game.camera.follow(worm.body);
						worm.applyDamage(worm.damageToTake);
						worm.damageToTake = null;
						if (i+1 == context.wormsDamaged.length){
						context.allWormsHarmed = true;
					}
						//context.wormsDamaged.splice(context.wormsDamaged.indexOf(worm), 1);
					},1000*i);
					
				}(i));
				
			}
			context.damagedWormsOnce = false;
			
								
			//context.allWormsHarmed = true;
		},
		
//***************************************************************************************************************************			
/* 		allWormsStationary: function(){
			var team = this._team1.getWholeTeam();	
			var team2 = this._team2.getWholeTeam();
			
			return (wholeTeamStationary(team) && wholeTeamStationary(team2));
		},
		
		wholeTeamStationary: function(team){
			for (var i = 0; i < team.length; i++){
				//console.log("Checking worm " + i + " on team "+teamIndex)
				if (team[i].isAlive()){
					if (!team[i].isStationary()){
						//console.log("Still worms on both teams");
						return false
					}
				}
			}
		}, */
//***************************************************************************************************************************		
		
		//temp
		progressTurn: function(game){
		/* 		//Turn increments for each PAIR of worms (team 1 worm and team 2 worm)
			if(this._team1Go == true && this._firstGoStopper == false){
				this._currentTurn++;
			} else{
				this._firstGoStopper=false;
			}
			
			//Reset turnCounter
			if(this._currentTurn > (this._team2.getWholeTeam().length)-1){
				this._currentTurn=0;
			}
			
			//Change worm - alternate team
			if(this._team1Go){
				if (!this._team1.getWorm(this._currentTurn).isAlive()){
					return this._team1.getWorm(this._currentTurn+1);
				}
				this._team1Go = ! this._team1Go;
				return this._team1.getWorm(this._currentTurn);
			} else {
				if (!this._team2.getWorm(this._currentTurn).isAlive()){
					return this._team2.getWorm(this._currentTurn+1);
				}
				this._team1Go = ! this._team1Go;
				return this._team2.getWorm(this._currentTurn);
			} */
			//console.log("************** TRIGGERED ****************");
			if (this.isWholeTeamDead(0) || this.isWholeTeamDead(1)){
				console.clear();
				//console.log("NO LIVING WORM");
				game.state.start("EndGame");
				return false;
			} else {
				var worm;
				
				//if end-of-team1 reached - reset counter
				if (this._currTeam1Index >= this._team1.getWholeTeam().length){
					this._currTeam1Index  = 0;
					//console.log("reset team 1");
				}
				
				//if end-of-team1 reached - reset counter
				if (this._currTeam2Index >= this._team2.getWholeTeam().length){
					this._currTeam2Index  = 0;
					//console.log("reset team 2");
				}
				
				//if it's team1s turn
				if (this._team1Go){
					//console.log("team 1s turn");
					//if the next worm in the team is alive
					if (this._team1.getWorm(this._currTeam1Index).isAlive()){
						//console.log("alive");
						//give turn to this worm
						worm = this._team1.getWorm(this._currTeam1Index);
						//console.log("returning team 1 at index " + this._currTeam1Index);
						//increase counter for next turn
						this._currTeam1Index++;
						
					} else {
						//if next worm in team is dead
						//console.log("next worm dead");
						var foundNext = false
						//find next alive worm after current
						for (var i = this._currTeam1Index; i < this._team1.getWholeTeam().length;i++){
							if (this._team1.getWorm(i).isAlive()){
								worm = this._team1.getWorm(i);
								this._currTeam1Index = i+1;
								foundNext = true;
							}
						}
						//if all worms after current are dead - find next alive worm before current
						if(!foundNext){
							for (i = 0; i< this._currTeam1Index ; i++){
								if (this._team1.getWorm(i).isAlive()){
									worm = this._team1.getWorm(i);
									this._currTeam1Index = i+1;
									foundNext = true;
								}
							}
						}
						if (!foundNext){
							console.clear();
							//console.log("NO LIVING WORM");
							return false;
						}
					}
				} else {
					//if team2s turn
					//console.log("team 2s turn");
					if (this._team2.getWorm(this._currTeam2Index).isAlive()){
						worm = this._team2.getWorm(this._currTeam2Index);
						console.log("returning team 2 at index " + this._currTeam2Index);
						this._currTeam2Index++;
					} else {
					//	console.log("next worm dead");
						/* if (this._currTeam2Index +1 !=this._team2.getWholeTeam().length && !(this._currTeam2Index +1 > this._team2.getWholeTeam().length)){
							console.log("moving to next worm in teamlist");
							this._currTeam2Index +=1;
							this.progressTurn();
							//worm = this._team2.getWorm(this._currTeam2Index);
							//this.progressTurn();
						} else {
							console.log("resetarting teamlist");
							this._currTeam2Index =0;
							this.progressTurn();
							//worm = this._team2.getWorm(this._currTeam2Index);
						} */
						var foundNext = false
						for (var i = this._currTeam2Index; i < this._team2.getWholeTeam().length;i++){
							if (this._team2.getWorm(i).isAlive()){
								worm = this._team2.getWorm(i);
								this._currTeam2Index = i+1;
								foundNext = true;
							}
						}
						if(!foundNext){
							for (i = 0; i< this._currTeam2Index ; i++){
								if (this._team2.getWorm(i).isAlive()){
									worm = this._team2.getWorm(i);
									this._currTeam2Index = i+1;
									foundNext = true;
								}
							}
						}
						if (!foundNext){
							
							console.clear();
							//console.log("NO LIVING WORM");
							game.state.start("EndGame");
							return false;
						}
					}
				}
			}
			this._team1Go = ! this._team1Go;
			//console.log("IS IT TEAM1's GO?: " + this._team1Go);
			return worm;
			
		},

	};