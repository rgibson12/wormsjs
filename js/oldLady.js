oldLady = function(id, sprite, power, splash, shotsAllowed, fuse, explosionRadius, weaponType,range, targetable){
		this._id = id;
		this._sprite = sprite;
		this._power = power;
		this.bulletExists = false;
		this.bullet;
		this._splash = splash;
		this._shotsAllowed = shotsAllowed;
		this._fuse = fuse;
		this._explosionRadius = explosionRadius;
		this._shotsInProgress = false;
		this._body;
		this._weaponType = weaponType;
		this._range = range;
		this._targetable = targetable;
		this._charging = false;
		this._maxDamage = 100;
	};
	
	oldLady.prototype = {
		getId: function(){
			return this._id;
		},
		
		showTargetting: function(context,game){
			
		},
		
		getTargetable(){
			return this._targetable;
		},
		
		getRange: function(){
			return this._range;
		},
		getWeaponType: function(){
			return this._weaponType;
		},
		getSprite: function(){
			return this._sprite;
		},
		getPower: function(){
			return this._power;
		},
		draw: function(game, x, y){
			this._body = game.add.sprite(x,y, this._sprite);
			return this._body;
		},
		getWidth: function(){
			return this._body.width;
		},
		getBody: function(){
			return this._body;
		},
		getShotsAllowed: function(){
			return this._shotsAllowed;
		},
		
		getFuse: function(){
			return this._fuse;
		},
		
		getExplosionRaidus: function(){
			return this._explosionRadius;
		},

		getShotsInProgress: function(){
			return this._shotsInProgress;
		},
		
		setShotsInProgress: function(shotsInProgress){
			this._shotsInProgress = shotsInProgress;
		},
		
		getChildren(){
			return undefined;
		},
		
		pathBullet: function(){
			
		},
		
		fire: function(context, game){
			if(!this.bulletExists){
				this.bulletExists = true;
				var p = new Phaser.Point(context.turret.x, context.turret.y);
					//the getWidth returns negative value due to negative rotation/scale/whatever..so only 1 case needed here
					//p.rotate(p.x, p.y, context.turret.rotation, false, this.getWidth());
				if (context.activeWorm.getFacingRight()){
					this.bullet = game.add.sprite(p.x+25, p.y, this._sprite);
				} else {
					this.bullet = game.add.sprite(p.x-25, p.y, this._sprite);
					
				}
				game.physics.box2d.enable(this.bullet);
				//this.bullet.body.setCircle(3);
			this.bullet.body.fixedRotation = true;
					p.setTo((Math.cos(context.turret.rotation) * context.firePower), (Math.sin(context.turret.rotation) * context.firePower));
				
				// Set velocity
				if(context.activeWorm.getFacingRight()){
					this.bullet.body.velocity.x = p.x;
					this.bullet.body.velocity.y = p.y;
				} else {
					this.bullet.body.velocity.x = -p.x;
					this.bullet.body.velocity.y = -p.y;
				}
				//this.bullet.body.setCategoryContactCallback(2, context.worldCollideCallback, context);
				//this.bullet.body.setCategoryContactCallback(3, context.worldCollideCallback, context);
				if (this._weaponType==0){
					game.camera.follow(this.bullet);}
				context.hitOnce = false;
				context.firePower = 0;
				context.exploded = false;
				this.bullet.walkRight = context.activeWorm.getFacingRight();
				this.bullet.rightSensor = this.bullet.body.addCircle(10, 8, 0);
				this.bullet.leftSensor = this.bullet.body.addCircle(10, -8, 0);
				
				this.bullet.body.setFixtureContactCallback(this.bullet.rightSensor, function(){this.setRightBlockedTrue();}, this);
				this.bullet.body.setFixtureContactCallback(this.bullet.leftSensor, function(){this.setLeftBlockedTrue();}, this);
				this.bullet.rightSensor.SetSensor(true);
				this.bullet.leftSensor.SetSensor(true);
				this._walkSound = game.add.audio('oldLady');
				this._walkSound.play("", 0, 1, true, true);
			}
		},
		
		setRightBlockedTrue: function(){
			console.log("old lady walk right");
			this.bullet.walkRight = false;
		},
		
		setLeftBlockedTrue: function(){
			console.log("old lady walk left");
			this.bullet.walkRight = true;
		},
		
		explodeBullet: function(context,game){
			this._walkSound.pause();
			console.log("EXPLODING BULLET FOR OLD LADY");
			if (this.bulletExists){
				context.timerInEffect = true;
			this.bullet.kill();
			this.bullet.destroy();
			context.bulletHitLand = false;	
			var oldVertices = context.groundVertices;
			var p = new Phaser.Point(this.bullet.x, this.bullet.y);

			context.explosion = new Phaser.Circle(p.x,p.y, this.getExplosionRaidus());			
			var newCirc = context.createCircle(20, p, this.getExplosionRaidus()/2);
			var subj_paths = new ClipperLib.Paths();
			
			for(var x = 0; x < context.currArrs.length; x++){
				var subj_path = new ClipperLib.Path();
				var toPush = context.currArrs[x];
				for (var i = 0; i< toPush.length; i+=2){
					subj_path.push(
						new ClipperLib.IntPoint(toPush[i], toPush[i+1]));
				}		
				subj_paths.push(subj_path);
				subj_path = null;
			}

			var clip_paths = new ClipperLib.Paths();
			var clip_path = new ClipperLib.Path();
			for(i=0; i < newCirc.length; i+=2){
				clip_path.push(
					new ClipperLib.IntPoint(Math.round(newCirc[i]), Math.round(newCirc[i+1])));
			}
			clip_paths.push(clip_path);
			var cpr = new ClipperLib.Clipper();
			
			var scale = 1;
			ClipperLib.JS.ScaleUpPaths(subj_paths, scale);
			ClipperLib.JS.ScaleUpPaths(clip_paths, scale);
			
			cpr.AddPaths(subj_paths, ClipperLib.PolyType.ptSubject, true);
			cpr.AddPaths(clip_paths, ClipperLib.PolyType.ptClip, true);
			var solution_paths =[];
			var clipType = ClipperLib.ClipType.ctDifference;
			var subject_fillType = ClipperLib.PolyFillType.pftNonZero;
			var clip_fillType = ClipperLib.PolyFillType.pftNonZero;
			var succeeded = cpr.Execute(clipType, solution_paths, subject_fillType, clip_fillType);
		
			var newBods = [];
			var newArrs = [];
			var toX, toY;
			context.groundBody.clearFixtures();
			if(!context.hitOnce){
				if(this.getWeaponType() <= 1){
				for (i = 0; i < solution_paths.length; i++){
					var newTerrain = [];
					var poly = solution_paths[i];
					for(var j = 0; j < poly.length; j++){
						var vert = poly[j];
						if (j == 0){
							toX = vert.X; 
							toY = vert.Y;
						}
						newTerrain.push(vert.X);
						newTerrain.push(vert.Y);	
					}
					//close the loop
					newTerrain.push(toX);
					newTerrain.push(toY);
					
					newBods.push(context.groundBody.addChain(newTerrain));	
					newArrs.push(newTerrain);
				}
				
				for (i = 0; i < context.currBods.length; i++){
					context.groundBody.removeFixture(context.currBods[i]);
				}
				context.currBods.splice(0, context.currBods.length);
				context.currArrs.splice(0, context.currArrs.length);
				for (i = 0; i < newBods.length; i++){
					context.currBods.push(newBods[i]);
				} 
				for (i = 0; i < newArrs.length; i++){
					context.currArrs.push(newArrs[i]);
				}	
				context.groundBody.setCollisionCategory(2);
				
				var x = Math.floor(this.bullet.x);
				var y = Math.floor(this.bullet.y);
				var rgba = context.land.getPixel(x, y);
				
				context.land.blendDestinationOut();
				context.land.circle(x, y, this.getExplosionRaidus()/2, 'rgba(0, 0, 0, 255');
				context.land.blendReset();
				context.land.update();
				context.land.addToWorld();			
				//MAX DX = 600 -> -600; MAX DY  = 700 -> -700;
				}
				this.effectWorms(1, game, context);
				
				
				//context.activeWorm.setShotsTaken(context.activeWorm.getShotsTaken() + 1);
				context.hitOnce = true;
			}			
			this.bulletExists=false;
			context.exploded = true;
			//context.fuseTimer.destroy();
			//add delay
			game.camera.follow(context.activeWorm.body);			
			context.weaponPanel.bringToFront();
			}
		},
		
		setCharging(charging){
			this._charging = charging;
		},
		
		getCharging(){
			return this._charging;
		},
		
		walk(){
			var walkFactor;
			if (this.bullet.walkRight){
				walkFactor = 50;
			} else{
				walkFactor = -50;
			}
			this.bullet.body.moveRight(walkFactor);
		},
		
		checkBulletLocation(){
			if (this.bulletExists){
				if (this.bullet.x > $(window).width()*2 || this.bullet.y > $(window).height() * 2){
					this.bullet.kill();
					this.bullet.destroy();
					this.bulletExists = false;
					return true;
				}
			}
			return false;
		},
		
		effectWorms: function(multiplier, game, context){
			for (var i = 0; i < context.turnSystem.getTeams().length; i++){
				for (var j = 0; j < context.turnSystem.getTeams()[i].getWholeTeam().length; j++){
					if (context.turnSystem.getTeams()[i].getWholeTeam()[j].isAlive()){
						//console.log("WIDTH: " + context.turnSystem.getTeams()[i].getWholeTeam()[j].getHeight());
						var topLeftY = context.turnSystem.getTeams()[i].getWholeTeam()[j].getY() - (context.turnSystem.getTeams()[i].getWholeTeam()[j].getHeight()/2);
						var topLeftX = context.turnSystem.getTeams()[i].getWholeTeam()[j].getX()-(context.turnSystem.getTeams()[i].getWholeTeam()[j].getWidth()/2);
						var width = context.turnSystem.getTeams()[i].getWholeTeam()[j].getWidth();
						var height = context.turnSystem.getTeams()[i].getWholeTeam()[j].getHeight();
						var lines = [
							new Phaser.Line(topLeftX, topLeftY, topLeftX + width, topLeftY),
							new Phaser.Line(topLeftX, topLeftY,topLeftX, topLeftY+height),
							new Phaser.Line(topLeftX + width, topLeftY, topLeftX+width, topLeftY+height),
							new Phaser.Line(topLeftX, topLeftY + height, topLeftX+width, topLeftY+height)
						];
						
						var centerX = topLeftX + (width/2);
						var centerY = topLeftY + (height/2);
						
						var intersects;
						//console.log("explosion: " + context.explosion.x + ", "+ context.explosion.y + ".... radius: " + context.explosion.radius);
						for(var p = 0; p < lines.length; p++) {
							var ar = lines[p].coordinatesOnLine();
							game.debug.geom(lines[p],'rgba(255,255,0, 1)');
							//console.log(p+ ":");
							for (var x = 0; x < ar.length; x++){
								//console.log(ar[x][0] + ", " + ar[x][1]);
								intersects = context.explosion.contains(ar[x][0], ar[x][1]);
								if (intersects) {
									break;
								}
							}
							if (intersects) {
								break;
							}
						}
						if (intersects){
							var dx = centerX-context.explosion.x ;
							var dy = centerY-context.explosion.y  ;
							var distEx = Math.sqrt((Math.round(dx)*Math.round(dx)) - (Math.round(dy)*Math.round(dy)));
							dx*=30;dy*=30;
							context.firePower = 0;			
								
							var dist = Math.abs(context.explosion.distance(new Phaser.Point(centerX,centerY)));
							var distDed = (dist/context.explosion.radius);
							var dmg = Math.round(this._maxDamage - (this._maxDamage * distDed));
							context.turnSystem.getTeams()[i].getWholeTeam()[j].applyDamage(dmg*multiplier);
							
				//TEMP***************************************************************************************
							context.turnSystem.getTeams()[i].getWholeTeam()[j].body.body.velocity.x = dx-(dx*distDed);
							context.turnSystem.getTeams()[i].getWholeTeam()[j].body.body.velocity.y= dy-(dy*distDed);
				//TEMP***************************************************************************************
						}
					}
				}
			}
		},
};