Skip = function(id, sprite, power, splash, shotsAllowed, fuse, explosionRadius, weaponType,range, targetable){
		this._id = id;
		this._sprite = sprite;
		this._power = power;
		this.bulletExists = false;
		this.bullet;
		this._splash = splash;
		this._shotsAllowed = shotsAllowed;
		this._fuse = fuse;
		this._explosionRadius = explosionRadius;
		this._shotsInProgress = false;
		this._body;
		this._weaponType = weaponType;
		this._range = range;
		this._targetable = targetable;
		this.bulletExists = false;
	};
	
	Skip.prototype = {
		getId: function(){
			return this._id;
		},
		
		getTargetable(){
			return this._targetable;
		},
		
		setCharging(charging){
			this._charging = charging;
		},
		
		getCharging(){
			return this._charging;
		},
		
		getRange: function(){
			return this._range;
		},
		getWeaponType: function(){
			return this._weaponType;
		},
		getSprite: function(){
			return this._sprite;
		},
		getPower: function(){
			return this._power;
		},
		
		walk(){
			//console.log("skipping");
		},
		
		draw: function(game, x, y){
			//var weapon = game.add.sprite(x,y, this._sprite);
			//console.log(weapon.width);
			this._body = game.add.sprite(x,y, this._sprite);
			return this._body;
		},
		getWidth: function(){
			return this._body.width;
		},
		getBody: function(){
			return this._body;
		},
		getShotsAllowed: function(){
			return this._shotsAllowed;
		},
		
		pathBullet: function(){
			
		},
		
		explodeBullet: function(context,game){
			context.timerInEffect = true;
			this.bulletExists = true;
			this.bullet = undefined;
		},
		
		getFuse: function(){
			return this._fuse;
		},
		
		getExplosionRaidus: function(){
			return this._explosionRadius;
		},

		getShotsInProgress: function(){
			return this._shotsInProgress;
		},
		
		setShotsInProgress: function(shotsInProgress){
			this._shotsInProgress = shotsInProgress;
		},
		
		showTargetting: function(context,game){
			
		},
		
		fire: function(args){
			if (!this.bulletExists){
				this.bullet = 1;
				//console.log("firing SKIP");
				//this.bulletExists = true;
			}
		},
		checkBulletLocation(){
			if (this.bulletExists){
				this.bulletExists = false;
				return true;
			}
		},
		
	};