Melee = function(id, sprite, power, splash, shotsAllowed, fuse, explosionRadius, weaponType,range, targetable){
		this._id = id;
		this._sprite = sprite;
		this._power = power;
		this.bulletExists = false;
		this.bullet;
		this._splash = splash;
		this._shotsAllowed = shotsAllowed;
		this._fuse = fuse;
		this._explosionRadius = explosionRadius;
		this._shotsInProgress = false;
		this._body;
		this._weaponType = weaponType;
		this._range = range;
		this._targetable = targetable;
		this._charging = true;
		this._maxDamage = 35;
	};
	
	Melee.prototype = {
		getId: function(){
			return this._id;
		},
		
		getTargetable(){
			return this._targetable;
		},
		
		setCharging(charging){
			//this._charging = charging;
		},
		
		getCharging(){
			return this._charging;
		},
		
		getRange: function(){
			return this._range;
		},
		getWeaponType: function(){
			return this._weaponType;
		},
		getSprite: function(){
			return this._sprite;
		},
		getPower: function(){
			return this._power;
		},
		draw: function(game, x, y){
			//var weapon = game.add.sprite(x,y, this._sprite);
			//console.log(weapon.width);
			this._body = game.add.sprite(x,y, this._sprite);
			return this._body;
		},
		getWidth: function(){
			return this._body.width;
		},
		getBody: function(){
			return this._body;
		},
		getShotsAllowed: function(){
			return this._shotsAllowed;
		},
		
		getFuse: function(){
			return this._fuse;
		},
		
		getExplosionRaidus: function(){
			return this._explosionRadius;
		},

		getShotsInProgress: function(){
			return this._shotsInProgress;
		},
		
		setShotsInProgress: function(shotsInProgress){
			this._shotsInProgress = shotsInProgress;
		},
		
		fire: function(context, game){
			if(!this.bulletExists){
				context.hitOnce = false;
				this.bulletExists = true;
				var p = new Phaser.Point(context.turret.x, context.turret.y);
					//the getWidth returns negative value due to negative rotation/scale/whatever..so only 1 case needed here
				p.rotate(p.x, p.y, context.turret.rotation, false, this.getWidth());
				
				this.bullet = game.add.sprite(p.x, p.y, 'bullet');
				game.physics.box2d.enable(this.bullet);
				this.bullet.body.setCircle(3);
				
					this.bullet.body.gravityScale=0;
					this.bullet.visible = false;
					p.setTo((Math.cos(context.turret.rotation) * context.firePower), (Math.sin(context.turret.rotation) * context.firePower));
				
				// Set velocity
				if(context.activeWorm.getFacingRight()){
					this.bullet.body.velocity.x = p.x;
					this.bullet.body.velocity.y = p.y;
				} else {
					this.bullet.body.velocity.x = -p.x;
					this.bullet.body.velocity.y = -p.y;
				}
				this.bullet.body.setCategoryContactCallback(2, context.worldCollideCallback, context);
				this.bullet.body.setCategoryContactCallback(3, context.worldCollideCallback, context);
				if (this._weaponType==0){
					game.camera.follow(this.bullet);}
				context.hitOnce = false;
				context.firePower = 0;
				context.exploded = false;
			}
		},
		
		showTargetting: function(context,game){
			
		},
		
		pathBullet: function(){
			
		},
		
		explodeBullet: function(context,game){
			console.log("EXPLODING BULLET FOR MELEE");
			if (this.bulletExists){
				context.timerInEffect = true;
				this.bullet.kill();
				this.bullet.destroy();
				context.bulletHitLand = false;	
				var oldVertices = context.groundVertices;
				
				var p = new Phaser.Point(context.turret.x, context.turret.y);

				context.explosion = new Phaser.Circle(p.x,p.y, context.turretPointer.length);	
				console.log(context.hitOnce + "HIT ONCE");
				if(!context.hitOnce){
					console.log("hitting worm");
					this.effectWorms(1,game,context);
					//context.activeWorm.setShotsTaken(context.activeWorm.getShotsTaken() + 1);
					context.hitOnce = true;
				} else {
					console.log("NOT Hitting worm");
				}		
				this.bulletExists=false;
				context.exploded = true;
				game.camera.follow(context.activeWorm.body);			
				context.weaponPanel.bringToFront();
			}
		},
		checkBulletLocation(){
			if (this.bulletExists){
				if (this.bullet.x > $(window).width()*2 || this.bullet.y > $(window).height() * 2){
					this.bullet.kill();
					this.bullet.destroy();
					this.bulletExists = false;
					return true;
				}
			}
		},
		
		effectWorms: function(multiplier, game, context){
			for (var i = 0; i < context.turnSystem.getTeams().length; i++){
				for (var j = 0; j < context.turnSystem.getTeams()[i].getWholeTeam().length; j++){
					if (context.turnSystem.getTeams()[i].getWholeTeam()[j] == context.activeWorm){
						continue;
					}
					else if (context.turnSystem.getTeams()[i].getWholeTeam()[j].isAlive()){
					//console.log("WIDTH: " + context.turnSystem.getTeams()[i].getWholeTeam()[j].getHeight());
					var topLeftY = context.turnSystem.getTeams()[i].getWholeTeam()[j].getY() - (context.turnSystem.getTeams()[i].getWholeTeam()[j].getHeight()/2);
					var topLeftX = context.turnSystem.getTeams()[i].getWholeTeam()[j].getX()-(context.turnSystem.getTeams()[i].getWholeTeam()[j].getWidth()/2);
					var width = context.turnSystem.getTeams()[i].getWholeTeam()[j].getWidth();
					var height = context.turnSystem.getTeams()[i].getWholeTeam()[j].getHeight();
					var lines = [
						new Phaser.Line(topLeftX, topLeftY, topLeftX + width, topLeftY),
						new Phaser.Line(topLeftX, topLeftY,topLeftX, topLeftY+height),
						new Phaser.Line(topLeftX + width, topLeftY, topLeftX+width, topLeftY+height),
						new Phaser.Line(topLeftX, topLeftY + height, topLeftX+width, topLeftY+height)
					];
					var centerX = topLeftX + (width/2);
					var centerY = topLeftY + (height/2);
					var intersects;
					//console.log("explosion: " + context.explosion.x + ", "+ context.explosion.y + ".... radius: " + context.explosion.radius);
					for(var p = 0; p < lines.length; p++) {
						//var ar = lines[p].coordinatesOnLine();
						//game.debug.geom(lines[p],'rgba(255,255,0, 1)');
						//console.log(p+ ":");
						
							//console.log(ar[x][0] + ", " + ar[x][1]);
							intersects = Phaser.Line.intersects(context.turretPointer, lines[p]);
							console.log("INTERSECTS: " + intersects);
							if (intersects!=undefined) {
								break;
							}
						
					}
								// Find the closest intersection
								//dist = game.math.distance(context.explosion.x, context.explosion.y, intersect.x, intersect.y);
							/* }
							
				}						if (context.explosion.contains(context.turnSystem.getTeams()[i].getWholeTeam()[j].getX(), context.turnSystem.getTeams()[i].getWholeTeam()[j].getY())){ */
						//console.log("WORM IN EXPLOSION");
					if (intersects!=undefined){
						if(context.activeWorm.getFacingRight()){	
							dx= 150; dy=50;
						} else {
							dx= -150; dy=-50;
						}
						//dx-=();'
						//dx*=context.firePower/10;
						//console.log("FirepwerL: " + context.firePower);
						//dy*=context.firePower/10;
						//dx = 600; dy =-700;
						context.firePower = 0;
					/* var lines = [
							new Phaser.Line(context.turnSystem.getTeams()[i].getWholeTeam()[j].getX(), context.turnSystem.getTeams()[i].getWholeTeam()[j].getY(), context.turnSystem.getTeams()[i].getWholeTeam()[j].getX() + context.turnSystem.getTeams()[i].getWholeTeam()[j].getWidth(), context.turnSystem.getTeams()[i].getWholeTeam()[j].getY()),
							new Phaser.Line(context.turnSystem.getTeams()[i].getWholeTeam()[j].getX(), context.turnSystem.getTeams()[i].getWholeTeam()[j].getY(), context.turnSystem.getTeams()[i].getWholeTeam()[j].getX(), context.turnSystem.getTeams()[i].getWholeTeam()[j].getY() + context.turnSystem.getTeams()[i].getWholeTeam()[j].getHeight()),
							new Phaser.Line(context.turnSystem.getTeams()[i].getWholeTeam()[j].getX() + context.turnSystem.getTeams()[i].getWholeTeam()[j].getWidth(), context.turnSystem.getTeams()[i].getWholeTeam()[j].getY(),
								context.turnSystem.getTeams()[i].getWholeTeam()[j].getX() + context.turnSystem.getTeams()[i].getWholeTeam()[j].getWidth(), context.turnSystem.getTeams()[i].getWholeTeam()[j].getY() + context.turnSystem.getTeams()[i].getWholeTeam()[j].getHeight()),
							new Phaser.Line(context.turnSystem.getTeams()[i].getWholeTeam()[j].getX(), context.turnSystem.getTeams()[i].getWholeTeam()[j].getY() + context.turnSystem.getTeams()[i].getWholeTeam()[j].getHeight(),
								context.turnSystem.getTeams()[i].getWholeTeam()[j].getX() + context.turnSystem.getTeams()[i].getWholeTeam()[j].getWidth(), context.turnSystem.getTeams()[i].getWholeTeam()[j].getY() + context.turnSystem.getTeams()[i].getWholeTeam()[j].getHeight())
						];
						
						for(var i = 0; i < lines.length; i++) {
							var intersect = Phaser.Line.intersects(context.explosion, lines[i]);
							if (intersect) {
								// Find the closest intersection
								dist = game.math.distance(context.explosion.x, context.explosion.y, intersect.x, intersect.y);
							}
						} */
						
			//TEMP - TODO: APPLY CALCULATED DAMAGE, NOT 50 **********************************************			
						
						context.turnSystem.getTeams()[i].getWholeTeam()[j].applyDamage(25);
						//dx-=(dx*distDed);
						//dy-=(dy*distDed);
						
			//TEMP***************************************************************************************
						context.turnSystem.getTeams()[i].getWholeTeam()[j].body.body.velocity.x = dx;
						context.turnSystem.getTeams()[i].getWholeTeam()[j].body.body.velocity.y= dy;
			//TEMP***************************************************************************************
					}
				}
				}
			}
		},		
};