
BasicGame.Preloader = function (game) {

	this.background = null;
	this.preloadBar = null;

	this.ready = false;

};

BasicGame.Preloader.prototype = {

	preload: function () {
		this.game.time.advancedTiming = true;
		//**WORM SPRITES**//
		this.game.load.image("tank", "assets/img/worm_base2.png");
		this.game.load.image("tombstone1", "assets/img/tombstone1.png");
		this.game.load.image("tombstone2", "assets/img/tombstone2.png");	
		this.game.load.image("tombstone3", "assets/img/tombstone3.png");	
		this.game.load.image("tombstone4", "assets/img/tombstone4.png");	
		this.game.load.image("tombstone5", "assets/img/tombstone5.png");	
		this.game.load.image("tombstone6", "assets/img/tombstone6.png");
		this.game.load.image("tank2", "assets/img/worm_base2_g.png");
		
		//**HUD SPRITES**//
		this.game.load.image("crosshair", "assets/img/cursor.png");
		this.game.load.image("targetCrosshair", "assets/img/cursor2.png");
		this.game.load.image('weaponPanel1', 'assets/img/weaponPanelBarX.png');
		this.game.load.image('weaponPanel2', 'assets/img/weaponPanelBar2.png');
		this.game.load.image('disabledWeaponPanel','assets/img/weaponPanelBar_disabled.png');
		this.game.load.image('timerContainer', 'assets/img/timerContainer.png');
		this.game.load.crossOrigin = 'anonymous';
		
		//**WEAPON SPRITES**//
		this.game.load.image("grenade", "assets/img/grenade2.png");
		this.game.load.image("drill", "assets/img/drill.png");
		this.game.load.image("oldLady", "assets/img/oldLady.png");
		this.game.load.image("clusterGrenade", "assets/img/clusterGrenade2.png");
		this.game.load.image('turret', 'assets/img/turret.png');
		this.game.load.image('bazooka', 'assets/img/bazooka.png');
		this.game.load.image('supersheep', 'assets/img/supersheep.png');
		this.game.load.image('sheep', 'assets/img/sheep.png');
		this.game.load.image('girder', 'assets/img/girder.png');
		this.game.load.image('girder_good', 'assets/img/girder_good.png');
		this.game.load.image('girder_bad', 'assets/img/girder_bad.png');
		this.game.load.image('homingMissile', 'assets/img/homing-missile.png');
		this.game.load.image('dynamite', 'assets/img/dynamite.png');
		this.game.load.spritesheet('mine', 'assets/img/minesprites.png', 14, 8);
		this.game.load.image('teleporter', 'assets/img/teleporter.png');
		this.game.load.image('airstrike', 'assets/img/airstrike.png');
		this.game.load.image('shotgun', 'assets/img/shotgun.png');
		this.game.load.image('bullet', 'assets/img/bullet.png');
		this.game.load.image('baseballBat', 'assets/img/baseballBat.png');
		this.game.load.image('background', 'assets/img/terrain2.png');
		this.game.load.image('flame', 'assets/img/flame.png');
		this.game.load.image('target', 'assets/img/target.png');
		this.game.load.image('land', 'assets/img/land.png');
		
		//**AUDIO ASSETS**//
		this.game.load.audio('wormWalk', 'assets/audio/Walk-Expand2.wav');
		this.game.load.audio('wormWalk2', 'assets/audio/Walk-Compress.wav');
		this.game.load.audio('oldLady', 'assets/audio/OLDWOMAN.wav');
		this.game.load.audio('airstrike', 'assets/audio/Airstrike.wav');
		this.game.load.audio('mineTick', 'assets/audio/MINETICK.wav');
		this.game.load.audio('mineDud', 'assets/audio/MINEDUD.wav');
	},

	create: function () {

        console.log("Creating preload state!");

		this.state.start('MainMenu');

	}

};
