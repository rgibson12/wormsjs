WeaponPanel = function(x, y){
		this._sprite = "weaponPanel";
		this._x	= x;
		this._y = y;
	};
	WeaponPanel.prototype = {
		show: function(game){
			this._weaponPanel = game.add.sprite(this._x,this._y, this._sprite);
			this._weaponPanel.anchor.setTo(0.5, 0.5);
			this._weaponPanel.fixedToCamera = true;
			this.bringToFront();
			//game.paused = true;
		},
		destroy: function(game){
			this._weaponPanel.destroy();
			//game.paused = false;
		},
		
		disable: function(){
			//this._weaponPanel.loadTexture('disabledWeaponPanel');
			this.bringToFront();
		},
		
		enable: function() {
			this._weaponPanel.loadTexture(this._sprite);	
			this.bringToFront();		
		},
		
		bringToFront: function(){
			this._weaponPanel.bringToTop();
		},

		resolveClick: function(event){
			var x1 = ($(window).width()/2) - 118;
			var x2 =($(window).width()/2) + 18;
			var y1 = ($(window).height()) - 31;
			var y2 = ($(window).height());
			
			if ((x1 < event.x && event.x <x2) && (y1 < event.y && event.y < y2)){
				var x = event.x - x1, y = event.y - y1;
				var changeWeaponIndex = undefined;
				//each weapon icon is 28x28 -> add 28 to the x for each check
				if (x<28 && y<28){	
					changeWeaponIndex = 0;
				}
				else if ((28< x && x<56) && y<28){
					changeWeaponIndex = 1;
				}
				//console.log("changing");
				//this.weaponPanel.destroy(this.game);
				//this.weaponPanel = undefined;
				return changeWeaponIndex;
			} else {
				//this.weaponPanel.destroy(this.game);
				//this.weaponPanel = undefined;
			}
		}
};