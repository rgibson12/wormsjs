Weapon = function(id, sprite, power, splash, shotsAllowed, fuse, explosionRadius, weaponType,range, targetable){
		this._id = id;
		this._sprite = sprite;
		this._power = power;
		this.bulletExists = false;
		this.bullet;
		this._splash = splash;
		this._shotsAllowed = shotsAllowed;
		this._fuse = fuse;
		this._explosionRadius = explosionRadius;
		this._shotsInProgress = false;
		this._body;
		this._weaponType = weaponType;
		this._range = range;
		this._targetable = targetable;
	};
	
	Weapon.prototype = {
		getId: function(){
			return this._id;
		},
		
		getTargetable(){
			return this._targetable;
		},
		
		getRange: function(){
			return this._range;
		},
		getWeaponType: function(){
			return this._weaponType;
		},
		getSprite: function(){
			return this._sprite;
		},
		getPower: function(){
			return this._power;
		},
		draw: function(game, x, y){
			//var weapon = game.add.sprite(x,y, this._sprite);
			//console.log(weapon.width);
			this._body = game.add.sprite(x,y, this._sprite);
			return this._body;
		},
		getWidth: function(){
			return this._body.width;
		},
		getBody: function(){
			return this._body;
		},
		getShotsAllowed: function(){
			return this._shotsAllowed;
		},
		
		getFuse: function(){
			return this._fuse;
		},
		
		getExplosionRaidus: function(){
			return this._explosionRadius;
		},

		getShotsInProgress: function(){
			return this._shotsInProgress;
		},
		
		setShotsInProgress: function(shotsInProgress){
			this._shotsInProgress = shotsInProgress;
		},
		
		fire: function(context, game){
			if(!this.bulletExists){
				this.bulletExists = true;
				var p = new Phaser.Point(context.turret.x, context.turret.y);
					//the getWidth returns negative value due to negative rotation/scale/whatever..so only 1 case needed here
					p.rotate(p.x, p.y, context.turret.rotation, false, this.getWidth());
				this.bullet = game.add.sprite(p.x, p.y, 'bullet');
				game.physics.box2d.enable(this.bullet);
				this.bullet.body.setCircle(3);
				if (this._weaponType==0){
					p.setTo((Math.cos(context.turret.rotation) * context.firePower), (Math.sin(context.turret.rotation) * context.firePower));
				} else {
					this.bullet.body.gravityScale=0;
					this.bullet.visible = false;
					p.setTo((Math.cos(context.turret.rotation) * context.firePower), (Math.sin(context.turret.rotation) * context.firePower));
				}
				// Set velocity
				if(context.activeWorm.getFacingRight()){
					this.bullet.body.velocity.x = p.x;
					this.bullet.body.velocity.y = p.y;
				} else {
					this.bullet.body.velocity.x = -p.x;
					this.bullet.body.velocity.y = -p.y;
				}
				this.bullet.body.setCategoryContactCallback(2, context.worldCollideCallback, context);
				this.bullet.body.setCategoryContactCallback(3, context.worldCollideCallback, context);
				if (this._weaponType==0){
					game.camera.follow(this.bullet);}
				context.hitOnce = false;
				context.firePower = 0;
				context.exploded = false;
			}
		},
};