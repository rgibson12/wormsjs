WeaponPanel = function(x, y){
		this._sprite = "weaponPanel1";
		this._x	= x;
		this._y = y;
		this._currShown = 1;
		this._maxPanels = 2;
		this.timerContainer;
	};
	WeaponPanel.prototype = {
		show: function(game){
			this._weaponPanel = game.add.sprite(this._x,this._y, this._sprite);
			this._weaponPanel.anchor.setTo(0.5, 0.5);
			this._weaponPanel.fixedToCamera = true;
			
			this.timerContainer = game.add.sprite($(window).width()-35,0,'timerContainer');
			this.timerContainer.fixedToCamera = true;
			
			this.bringToFront();
			//game.paused = true;
		},
		destroy: function(game){
			this._weaponPanel.destroy();
			//game.paused = false;
		},
		
		disable: function(){
			this._weaponPanel.loadTexture('disabledWeaponPanel');
			this.bringToFront();
		},
		
		enable: function() {
			this._weaponPanel.loadTexture(this._sprite);	
			this.bringToFront();		
		},
		
		bringToFront: function(){
			this._weaponPanel.bringToTop();
			this.timerContainer.bringToTop();
		},
		
		progress: function(){
			if(this._currShown+1 > this._maxPanels){
				this._currShown = 1;
			} else {
				this._currShown++;
			}
				
			this._sprite = "weaponPanel"+this._currShown;
			this._weaponPanel.loadTexture(this._sprite);
		},
		
		regress: function(){
			if(this._currShown-1 < 1){
				this._currShown = 2;
			} else {
				this._currShown--;
			}	
			this._sprite = "weaponPanel"+this._currShown;
			this._weaponPanel.loadTexture(this._sprite);
		},

		resolveClick: function(x, currIndex){
			var changeWeaponIndex = undefined;
			//each weapon icon is 28x28 -> add 28 to the x for each check
			if (x<28){	
				changeWeaponIndex = 0;
			}
			else if ((28< x && x<56)){
				changeWeaponIndex = 1;
			}
			else if ((56< x && x<84)){
				changeWeaponIndex = 2;
			} 
			else if((84<x && x<112)){
				changeWeaponIndex = 3;
			}
			else if((112<x && x<140)){
				changeWeaponIndex = 4;
			}
			else if (140<x && x<168){
				changeWeaponIndex = 5;
			}
			else if (168<x && x<196){
				changeWeaponIndex = 6;
			} 
			else if (196< x && x < 224){
				changeWeaponIndex = 7;				
			}
			
			changeWeaponIndex += 8*(this._currShown-1)
			
			return changeWeaponIndex;
			
		}
};