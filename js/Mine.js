Mine = function(id, sprite, power, x,y, game, context){
		this._id = id;
		this._sprite = sprite;
		this._power = power;
		this.bulletExists = true;
		this._explosionRadius = 55
		this._active = true;
		
		this.bullet = game.add.sprite(x, y, this._sprite);
		game.physics.box2d.enable(this.bullet);
		this.bullet.body.setRectangle(13,8);
		this.bullet.wormSensor = this.bullet.body.addCircle(25, 0, 0);
		this.bullet.wormSensor.SetSensor(true);
		this.bullet.body.setCollisionCategory(4);
		this.bullet.animations.add('flash',[0,1], true);
		this.game;
		this.context;
		
		this._maxDamage = 40;
		
		this._tickSound = game.add.audio('mineTick');
		this._dudSound = game.add.audio('mineDud');
	};
	
	Mine.prototype = {
		getId: function(){
			return this._id;
		},
		
		getActive: function(){
			return this._active;
		},
		
		getTargetable(){
			return this._targetable;
		},
		
		getRange: function(){
			return this._range;
		},
		getWeaponType: function(){
			return this._weaponType;
		},
		getSprite: function(){
			return this._sprite;
		},
		getPower: function(){
			return this._power;
		},
		draw: function(game, x, y){
			//var weapon = game.add.sprite(x,y, this._sprite);
			//console.log(weapon.width);
			this._body = game.add.sprite(x,y, this._sprite);
			return this._body;
		},
		getWidth: function(){
			return this._body.width;
		},
		getBody: function(){
			return this._body;
		},
		getShotsAllowed: function(){
			return this._shotsAllowed;
		},
		
		getFuse: function(){
			return this._fuse;
		},
		
		getExplosionRaidus: function(){
			return this._explosionRadius;
		},

		getShotsInProgress: function(){
			return this._shotsInProgress;
		},
		
		setShotsInProgress: function(shotsInProgress){
			this._shotsInProgress = shotsInProgress;
		},
		
		getChildren(){
			return undefined;
		},
		
		explodeBullet: function(context,game){

			this.context = context;
			this.game = game;
			this._tickSound.play("", 0, 0.1, true, false);
			var fuse = game.time.create(game, true);
			this.bullet.animations.play('flash',5,true);
			fuse.add(4000, function(){
				var decision = Math.floor((Math.random() * 10) + 1); 
				if (decision <=6){
					if (this.bulletExists){
						//console.log("EXPLODING BULLET FOR MINE");
						this._active = false;
						this.bullet.kill();
						this.bullet.destroy();
						//this.context.bulletHitLand = false;	
						var oldVertices = this.context.groundVertices;
						var p = new Phaser.Point(this.bullet.x, this.bullet.y);

						this.context.explosion = new Phaser.Circle(p.x,p.y, this.getExplosionRaidus());			
						var newCirc = this.context.createCircle(20, p, this.getExplosionRaidus()/2);
						var subj_paths = new ClipperLib.Paths();
						
						for(var x = 0; x < this.context.currArrs.length; x++){
							var subj_path = new ClipperLib.Path();
							var toPush = this.context.currArrs[x];
							for (var i = 0; i< toPush.length; i+=2){
								subj_path.push(
									new ClipperLib.IntPoint(toPush[i], toPush[i+1]));
							}		
							subj_paths.push(subj_path);
							subj_path = null;
						}

						var clip_paths = new ClipperLib.Paths();
						var clip_path = new ClipperLib.Path();
						for(i=0; i < newCirc.length; i+=2){
							clip_path.push(
								new ClipperLib.IntPoint(Math.round(newCirc[i]), Math.round(newCirc[i+1])));
						}
						clip_paths.push(clip_path);
						var cpr = new ClipperLib.Clipper();
						
						var scale = 1;
						ClipperLib.JS.ScaleUpPaths(subj_paths, scale);
						ClipperLib.JS.ScaleUpPaths(clip_paths, scale);
						
						cpr.AddPaths(subj_paths, ClipperLib.PolyType.ptSubject, true);
						cpr.AddPaths(clip_paths, ClipperLib.PolyType.ptClip, true);
						var solution_paths =[];
						var clipType = ClipperLib.ClipType.ctDifference;
						var subject_fillType = ClipperLib.PolyFillType.pftNonZero;
						var clip_fillType = ClipperLib.PolyFillType.pftNonZero;
						var succeeded = cpr.Execute(clipType, solution_paths, subject_fillType, clip_fillType);
					
						var newBods = [];
						var newArrs = [];
						var toX, toY;
						this.context.groundBody.clearFixtures();
						
							
							for (i = 0; i < solution_paths.length; i++){
								var newTerrain = [];
								var poly = solution_paths[i];
								for(var j = 0; j < poly.length; j++){
									var vert = poly[j];
									if (j == 0){
										toX = vert.X; 
										toY = vert.Y;
									}
									newTerrain.push(vert.X);
									newTerrain.push(vert.Y);	
								}
								newTerrain.push(toX);
								newTerrain.push(toY);
								
								newBods.push(this.context.groundBody.addChain(newTerrain));	
								newArrs.push(newTerrain);
							}
							
							for (i = 0; i < this.context.currBods.length; i++){
								this.context.groundBody.removeFixture(this.context.currBods[i]);
							}
							this.context.currBods.splice(0, this.context.currBods.length);
							this.context.currArrs.splice(0, this.context.currArrs.length);
							for (i = 0; i < newBods.length; i++){
								this.context.currBods.push(newBods[i]);
							} 
							for (i = 0; i < newArrs.length; i++){
								this.context.currArrs.push(newArrs[i]);
							}	
							this.context.groundBody.setCollisionCategory(2);
							
							var x = Math.floor(this.bullet.x);
							var y = Math.floor(this.bullet.y);
							var rgba = this.context.land.getPixel(x, y);
							
							this.context.land.blendDestinationOut();
							this.context.land.circle(x, y, this.getExplosionRaidus()/2, 'rgba(0, 0, 0, 255');
							this.context.land.blendReset();
							this.context.land.update();
							this.context.land.addToWorld();			
							//MAX DX = 600 -> -600; MAX DY  = 700 -> -700;
							
							this.effectWorms(1, this.game, this.context);
							
							
							//this.//context.activeWorm.setShotsTaken(this.context.activeWorm.getShotsTaken() + 1);
							//this.context.hitOnce = true;
							
						this.bulletExists=false;
						//this.context.exploded = true;
						//this.context.fuseTimer.destroy();
						//add delay
						this.game.camera.follow(this.context.activeWorm.body);			
						this.context.weaponPanel.bringToFront();
						this._tickSound.stop("", 0, 1, true, false);
					}
				} else {
					console.log("mine shortfused");
					this._tickSound.stop("", 0, 1, true, false);
					this._dudSound.play("", 0, 1, false, false)
					this.bullet.animations.stop('flash',true);
				}
			
			}, this);
			fuse.start();

		},
		
		setCharging(charging){
			this._charging = charging;
		},
		
		getCharging(){
			return this._charging;
		},
		
		checkBulletLocation(){
			if (this.bulletExists){
				if (this.bullet.x > $(window).width()*2 || this.bullet.y > $(window).height() * 2){
					this.bullet.kill();
					this.bullet.destroy();
					this.bulletExists = false;
					return true;
				}
			}
			return false;
		},
		
		effectWorms: function(multiplier, game, context){
			for (var i = 0; i < context.turnSystem.getTeams().length; i++){
				for (var j = 0; j < context.turnSystem.getTeams()[i].getWholeTeam().length; j++){
					if (context.turnSystem.getTeams()[i].getWholeTeam()[j].isAlive()){
						//console.log("WIDTH: " + context.turnSystem.getTeams()[i].getWholeTeam()[j].getHeight());
						var topLeftY = context.turnSystem.getTeams()[i].getWholeTeam()[j].getY() - (context.turnSystem.getTeams()[i].getWholeTeam()[j].getHeight()/2);
						var topLeftX = context.turnSystem.getTeams()[i].getWholeTeam()[j].getX()-(context.turnSystem.getTeams()[i].getWholeTeam()[j].getWidth()/2);
						var width = context.turnSystem.getTeams()[i].getWholeTeam()[j].getWidth();
						var height = context.turnSystem.getTeams()[i].getWholeTeam()[j].getHeight();
						var lines = [
							new Phaser.Line(topLeftX, topLeftY, topLeftX + width, topLeftY),
							new Phaser.Line(topLeftX, topLeftY,topLeftX, topLeftY+height),
							new Phaser.Line(topLeftX + width, topLeftY, topLeftX+width, topLeftY+height),
							new Phaser.Line(topLeftX, topLeftY + height, topLeftX+width, topLeftY+height)
						];
						
						var centerX = topLeftX + (width/2);
						var centerY = topLeftY + (height/2);
						
						var intersects;
						//console.log("explosion: " + context.explosion.x + ", "+ context.explosion.y + ".... radius: " + context.explosion.radius);
						for(var p = 0; p < lines.length; p++) {
							var ar = lines[p].coordinatesOnLine();
							game.debug.geom(lines[p],'rgba(255,255,0, 1)');
							//console.log(p+ ":");
							for (var x = 0; x < ar.length; x++){
								//console.log(ar[x][0] + ", " + ar[x][1]);
								intersects = context.explosion.contains(ar[x][0], ar[x][1]);
								if (intersects) {
									break;
								}
							}
							if (intersects) {
								break;
							}
						}
									// Find the closest intersection
									//dist = game.math.distance(context.explosion.x, context.explosion.y, intersect.x, intersect.y);
								/* }
								
					}						if (context.explosion.contains(context.turnSystem.getTeams()[i].getWholeTeam()[j].getX(), context.turnSystem.getTeams()[i].getWholeTeam()[j].getY())){ */
							//console.log("WORM IN EXPLOSION");
						if (intersects){
							var dx =  centerX-context.explosion.x ;
							var dy = centerY-context.explosion.y  ;
							var distEx = Math.sqrt((Math.round(dx)*Math.round(dx)) - (Math.round(dy)*Math.round(dy)));
									console.log("distance should be: " + distEx);
							//console.log(dx + ", " + dy);
							dx*=30;dy*=30;	
							//dx-=();'
							//dx*=context.firePower/10;
							//console.log("FirepwerL: " + context.firePower);
							//dy*=context.firePower/10;
							//dx = 600; dy =-700;
							context.firePower = 0;
						/* var lines = [
								new Phaser.Line(context.turnSystem.getTeams()[i].getWholeTeam()[j].getX(), context.turnSystem.getTeams()[i].getWholeTeam()[j].getY(), context.turnSystem.getTeams()[i].getWholeTeam()[j].getX() + context.turnSystem.getTeams()[i].getWholeTeam()[j].getWidth(), context.turnSystem.getTeams()[i].getWholeTeam()[j].getY()),
								new Phaser.Line(context.turnSystem.getTeams()[i].getWholeTeam()[j].getX(), context.turnSystem.getTeams()[i].getWholeTeam()[j].getY(), context.turnSystem.getTeams()[i].getWholeTeam()[j].getX(), context.turnSystem.getTeams()[i].getWholeTeam()[j].getY() + context.turnSystem.getTeams()[i].getWholeTeam()[j].getHeight()),
								new Phaser.Line(context.turnSystem.getTeams()[i].getWholeTeam()[j].getX() + context.turnSystem.getTeams()[i].getWholeTeam()[j].getWidth(), context.turnSystem.getTeams()[i].getWholeTeam()[j].getY(),
									context.turnSystem.getTeams()[i].getWholeTeam()[j].getX() + context.turnSystem.getTeams()[i].getWholeTeam()[j].getWidth(), context.turnSystem.getTeams()[i].getWholeTeam()[j].getY() + context.turnSystem.getTeams()[i].getWholeTeam()[j].getHeight()),
								new Phaser.Line(context.turnSystem.getTeams()[i].getWholeTeam()[j].getX(), context.turnSystem.getTeams()[i].getWholeTeam()[j].getY() + context.turnSystem.getTeams()[i].getWholeTeam()[j].getHeight(),
									context.turnSystem.getTeams()[i].getWholeTeam()[j].getX() + context.turnSystem.getTeams()[i].getWholeTeam()[j].getWidth(), context.turnSystem.getTeams()[i].getWholeTeam()[j].getY() + context.turnSystem.getTeams()[i].getWholeTeam()[j].getHeight())
							];
							
							for(var i = 0; i < lines.length; i++) {
								var intersect = Phaser.Line.intersects(context.explosion, lines[i]);
								if (intersect) {
									// Find the closest intersection
									dist = game.math.distance(context.explosion.x, context.explosion.y, intersect.x, intersect.y);
								}
							} */
							
				//TEMP - TODO: APPLY CALCULATED DAMAGE, NOT 50 **********************************************			
							var dist = Math.abs(context.explosion.distance(new Phaser.Point(centerX,centerY)));
								var distDed = (dist/context.explosion.radius);
								var dmg = Math.round(this._maxDamage - (this._maxDamage * distDed));
								context.turnSystem.getTeams()[i].getWholeTeam()[j].applyDamage(dmg*multiplier);
							//dx-=(dx*distDed);
							//dy-=(dy*distDed);
							
				//TEMP***************************************************************************************
							context.turnSystem.getTeams()[i].getWholeTeam()[j].body.body.velocity.x = dx-(dx*distDed);
							context.turnSystem.getTeams()[i].getWholeTeam()[j].body.body.velocity.y= dy-(dy*distDed);
				//TEMP***************************************************************************************
						}
					}
				}
			}
		},
};