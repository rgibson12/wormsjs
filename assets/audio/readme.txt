------------------------------------------------------------------------------------
Title: Ryan's Worms Sound Effect Pack
Author: Ryan Williams
Website: http://www.ryansgoblog.com/
Release Date: October 29th, 2004
------------------------------------------------------------------------------------

This is a collection of sound effects that replace those included with Worms World
Party or Worms Armageddon by default. To install, simply move everything in this 
zip file to the below directory:

C:\Worms Armageddon\DATA\Wav\Effects

You may wish to save a copy of your original sound effects as these will overwrite 
the originals. The above directory may also vary depending on where your copy of
Worms is installed. Just look for a folder full of .wav files matching the names 
in this .zip file and you know'll know you're on the right track.

If you'd like more information about this sound pack, please see the following URL:

http://www.ryansgoblog.com/2006/11/08/ryans-worms-sound-effect-pack/

You should always be able to find a current, working mirror at the above URL.

------------------------------------------------------------------------------------