BasicGame.Game = function (game) {

		this.groundVertices = [37,200,39,208,39,223,36,234,35,238,31,243,30,248,36,261,30,269,29,280,30,296,23,300,18,307,18,340,
			6,360,5,392,19,429,20,453,21,463,31,500,16,522,13,545,23,578,350,578,352,519,359,494,355,483,349,475,
			345,474,336,473,336,460,351,411,350,400,347,393,342,388,339,379,338,363,322,333,302,305,274,258,266,255,
			252,252,248,238,246,225,237,209,229,179,221,179,220,159,217,146,202,130,191,125,180,124,179,130,174,134,
			171,142,141,142,126,151,126,189,97,216,98,246,101,275,80,298,77,310,75,324,68,330,67,319,63,310,55,297,
			60,289,60,280,55,269,59,263,62,255,60,247,64,245,65,242,59,238,61,221,68,217,67,213,47,200,37,200];
		this.x=0; this.y=0; this.i=1; this.r=0;
		this.powerCol = 122;
		this.timeStamp = 0;
		this.explosion = null;
		this.hitOnce = false;
		this.opacity =0 ;
		this.turretPointer;
		this.tFacingRight = true;
		this.cursors;
		this.jumpButton;
		this.turret;
		this.bulletExists=false;
		this.bulletHitLand = false;
		this.fireBut;
		this.activeWorm;
		this.changeTimer = 0;
		this.powerIndicator = [];
		this.fireBallR;
		this.currBods = [];
		this.currArrs = [];
		this.bullet;
		this.arsenal = [];
		this.groundBody = null;
		this.selectedWeapon;
		this.land;
		this.weaponChange;
		this.weaponPanel;
		this.weaponButton;
		this.weaponPanelTimer=0;
		this.debugOn = false;
		this.debugOnBut;
		this.firePower = 0;
		this.fireDown = false;
		this.turnSystem;
		this.crossHair;
		this.exploded = false;
		//this.game.paused = false;	
    };

    BasicGame.Game.prototype = {
		createArsenal: function(){
		/* 	this.bazooka = Object.create(Weapon);//('bazooka', 900, false);
			this.bazooka.initialize(0,'bazooka', 900, false);
			this.normal = Object.create(Weapon);//new Weapon('turret', 600, false);
			this.normal.initialize(1,'turret', 600, false);
			this.arsenal.push(this.bazooka);
			this.arsenal.push(this.normal); */
			this.arsenal = new Arsenal();
			//this.arsenal.initialize();
			//this.arsenal.addWeapon(this.bazooka);
			//this.arsenal.addWeapon(this.normal);
			this.selectedWeapon = this.arsenal.getWeapon(0);
			this.firePower = 0;	
		},
		
		createLand: function(){
			this.land = this.add.bitmapData(1500, 500);
            this.land.draw('background');
            this.land.update();
            this.land.addToWorld();
		},

        create: function () {
			this.createLand();
			this.createArsenal();
			this.game.world.setBounds(0, 0, 1000, 1000);
			this.currArrs.push(this.groundVertices);
			
			this.game.stage.backgroundColor = '#124184';
			// Enable Box2D physics
			this.game.physics.startSystem(Phaser.Physics.BOX2D);
			this.game.physics.startSystem(Phaser.Physics.ARCADE);
			this.game.physics.box2d.gravity.y = 500;
			this.game.physics.box2d.friction = 0.8;
			for (var i = 0; i < this.groundVertices.length; i++){			
				this.groundVertices[i] = Math.round(this.groundVertices[i]);
			}
			
			// Make the ground body
			this.groundBody = new Phaser.Physics.Box2D.Body(this.game, null, 0, 0, 0);
			this.currBods.push(this.groundBody.setChain(this.groundVertices));
			this.groundBody.setCollisionCategory(2);	

			this.cursors = this.game.input.keyboard.createCursorKeys();
			this.fireBut = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
			this.jumpButton = this.game.input.keyboard.addKey(Phaser.Keyboard.J);
			this.weaponChange = this.game.input.keyboard.addKey(Phaser.Keyboard.C);
			this.weaponButton = this.game.input.keyboard.addKey(Phaser.Keyboard.W);
			this.fireBut.onDown.add(this.charge, this);
			this.fireBut.onUp.add(this.fire, this);
			this.game.input.onDown.add(this.changeWeapon,this);
			this.showWeaponPanel();
			this.debugOnBut = this.game.input.keyboard.addKey(Phaser.Keyboard.D);
			this.debugOnBut.onDown.add(this.debug, this);
			//var caption = this.game.add.text(5, 5, 'Simple car control. Left/right arrow keys to move, down arrow key to brake.', { fill: '#ffffff', font: '14pt Arial' });
			//caption.fixedToCamera = true;
			
			//this.activeWorm.body = this.game.add.sprite(100, 50, 'tank');
			
			/* this.activeWorm= Object.create(Worm);
			this.activeWorm.initialize(100, 50, this.game, this); */
			//this.team1 = Object.create(Team);
			//this.activeWorm = this.team1.initialize(this.game, this);
			this.turnSystem = new Turnsystem(this.game, this);
			this.activeWorm = this.turnSystem.getStartingWorm();
			//t = w.draw(this.game);
			//this.activeWorm.body.draw(this.game);
			//this.point = new Phaser.Circle(this.activeWorm.getX() + 50, this.activeWorm.getY() + 50, 15);
			//this.activeWorm2 = this.add.sprite(300, 0, 'tank');
			/* 	this.game.physics.enable([this.activeWorm.body], Phaser.Physics.BOX2D);
			this.activeWorm.body.body.fixedRotation = true;
			this.activeWorm.syncX();
			this.activeWorm.syncY();
			//this.activeWorm2.body.fixedRotation = true;
			this.activeWorm.body.body.setCategoryContactCallback(2, this.resetcanJump, this); */
			this.createTurret();			
			this.turret.visible = false;
			
			this.crosshair.visible = false;
			this.game.camera.follow(this.activeWorm.body);
			this.timeStamp = this.game.time.now;
        },
		
		debug: function(){
			this.debugOn = !this.debugOn;
			this.render();
		},
		
		createTurret: function(){
			//console.log(this.selectedWeapon.getSprite());
			this.turret = this.selectedWeapon.draw(this.game, this.activeWorm.getX(),  this.activeWorm.getY());
			this.turret.rotation = 0;
			this.activeWorm.setFacingRight(true);
			//if(this.)			
			this.turretPointer = new Phaser.Line(this.turret.x+this.selectedWeapon.getWidth(), this.turret.y-5, this.turret.x+70, this.turret.y);
			this.crosshair = this.game.add.sprite(this.turret.x+90,this.turret.y,'crosshair');
			
			//this.game.physics.enable(this.turret, Phaser.Physics.ARCADE);
		},

		resetcanJump: function(){
			this.activeWorm.resetcanJump();
		},
		
		charge: function(){
			if(!this.selectedWeapon.getIsProjectile()){
				this.firePower = this.selectedWeapon.getPower();
				console.log("not charging");
			} else {
			this.fireDown = true;
			this.fireBallR = 10;
			this.powerIndicator = [];}
		},
		
		fire: function (){
			this.powerIndicator.splice(0, this.powerIndicator.length);
			this.fireDown = false;
			this.selectedWeapon.setShotsInProgress(true);
			if (this.selectedWeapon.getShotsAllowed()>1){
				this.weaponPanel.disable();
			}
			this.opacity = 0;
			if(!this.bulletExists){
				this.bulletExists = true;
				var p = new Phaser.Point(this.turret.x, this.turret.y);
				//if(this.activeWorm.getFacingRight()){
					//the getWidth returns negative value due to negative rotation/scale/whatever..so only 1 case needed here
					p.rotate(p.x, p.y, this.turret.rotation, false, this.selectedWeapon.getWidth());
					//console.log("THIS IS THE LENGTH OF THE WEAPON: " + this.selectedWeapon.getWidth());
				 /* else {
					p.rotate(p.x, p.y, this.turret.rotation, false, this.selectedWeapon.getWidth());
				} */
				this.bullet = this.game.add.sprite(p.x, p.y, 'bullet');
				this.game.physics.box2d.enable(this.bullet);
				this.bullet.body.setCircle(5);
				if (this.selectedWeapon.getIsProjectile()){
					p.setTo((Math.cos(this.turret.rotation) * this.firePower), (Math.sin(this.turret.rotation) * this.firePower));
				} else {
					this.bullet.body.gravityScale=0;
					this.bullet.visible = false;
					p.setTo((Math.cos(this.turret.rotation) * this.firePower), (Math.sin(this.turret.rotation) * this.firePower));
				}
				// Set velocity
				if(this.activeWorm.getFacingRight()){
					this.bullet.body.velocity.x = p.x;
					this.bullet.body.velocity.y = p.y;
				} else {
					this.bullet.body.velocity.x = -p.x;
					this.bullet.body.velocity.y = -p.y;
				}
				this.bullet.body.setCategoryContactCallback(2, this.worldCollideCallback, this);
				this.game.camera.follow(this.bullet);
				this.hitOnce = false;
				this.firePower = 0;
				this.exploded = false;
			}
		},
 
		worldCollideCallback: function() {
			var fuseTimer = this.game.time.create(this.game, true);
			this.bulletHitLand = true;
			fuseTimer.add(this.selectedWeapon.getFuse(), this.doExplosion, this);
			fuseTimer.start();
		},

		update: function() {	
			this.game.debug.geom(new Phaser.Point(this.activeWorm.getX(), this.activeWorm.getY()),'rgba(255,255,0, 1)');
			if (this.bulletExists && !this.selectedWeapon.getIsProjectile()){
				if (Math.abs(this.bullet.x - this.turret.x) > this.selectedWeapon.getRange() || Math.abs(this.bullet.y- this.turret.y) > this.selectedWeapon.getRange()  ){
					this.doExplosion();
				}
			}
			for (var i = 0; i < this.turnSystem.getTeams().length; i++){
				this.turnSystem.getTeams()[i].alignHpLabels();
			}	
			if ((this.game.time.now - this.timeStamp >= 60000) || (this.activeWorm.getShotsTaken() >= this.selectedWeapon.getShotsAllowed())&& this.exploded){
				this.changeWorm();
			}
			if (this.fireDown){
				var opacityMulti = (this.selectedWeapon.getPower())/10;
				if (this.firePower < this.selectedWeapon.getPower() ){
					this.firePower+=10;
					//this.powerIndicator.destroy();
					this.opacity +=(1/opacityMulti);
					
					var ar = [];
					this.turretPointer.coordinatesOnLine(1, ar);
					if (this.powerIndicator[0] == undefined){
						this.powerIndicator.push(new Phaser.Circle(ar[0].x, ar[0].y, 5));
						this.x = this.turret.x;
						this.y = this.turret.y-25;
						this.r = 5;
						this.i=1;
					} else {
						this.r+=0.2;
						if(this.i < ar.length){
							this.powerIndicator.push(new Phaser.Circle(ar[this.i][0],ar[this.i][1], this.r));
						}
						this.i++;
					}
					this.powerCol++;
					//this.powerIndicator[0].setTo(t);
					//this.fireBallR += 0.25
					//this.powerIndicator.setTo(this.activeWorm.getX()+45, this.activeWorm.getY(), this.fireBallR);	
				}
			}
			if (this.cursors.left.isDown && !this.cursors.right.isDown) { 
				this.activeWorm.move(-50); 
				this.turret.visible = false; 
				this.crosshair.visible = false; 
				this.turShown = false; 
				this.activeWorm.setFacingRight(false);
			}
			else if (this.cursors.right.isDown && !this.cursors.left.isDown) { 
				this.activeWorm.move(50); 
				this.turret.visible = false; 
				this.crosshair.visible = false;
				this.turShown = false; 
				this.activeWorm.setFacingRight(true);
			}
			else if (this.cursors.up.isDown && this.activeWorm.body.body.velocity.x==0){
				if(this.activeWorm.getFacingRight() ){
					this.turret.angle--;
				}
				else if (!this.activeWorm.getFacingRight() ){
					this.turret.angle++;
				}
			}
			else if (this.cursors.down.isDown && this.activeWorm.body.body.velocity.x==0) { 
				if(this.activeWorm.getFacingRight() ){
					this.turret.angle++;
				} else if (!this.activeWorm.getFacingRight()){
					this.turret.angle--;
				}
			}
			if (this.jumpButton.isDown && this.activeWorm.getcanJump()) {
				this.activeWorm.resetcanJump();
				this.activeWorm.jump();
				console.log(this.activeWorm.getcanJump());	
			}
			else { 
				if(this.activeWorm.body.body.velocity.x === 0 && this.activeWorm.body.body.velocity.y ===0 && this.activeWorm.getcanJump()){
					this.turret.visible = true;
					this.crosshair.visible = true;
					this.turret.x = this.activeWorm.getX();
					this.turret.y = this.activeWorm.getY();
					
					this.turret.anchor.setTo(0,0.5);
					this.crosshair.anchor.setTo(0.5,0.5);
					if(this.activeWorm.getFacingRight()){
						if(!this.tFacingRight){
							this.turret.scale.x = 1;
							this.turret.rotation = - this.turret.rotation;
							this.tFacingRight = true;
						}
					} else {
						if(this.tFacingRight){
							this.turret.scale.x = -1;
							this.turret.rotation = - this.turret.rotation;
							this.tFacingRight = false;
						}
					}
					
					var p = new Phaser.Point(this.activeWorm.getX(),this.activeWorm.getY());
					p.rotate(p.x, p.y, this.turret.rotation,false,this.selectedWeapon.getWidth());
					if (this.activeWorm.getFacingRight()){
						this.turretPointer.fromAngle(p.x,p.y,this.turret.rotation, 70);
						p.rotate(p.x, p.y, this.turret.rotation,false,90)
					} else {
						this.turretPointer.fromAngle(p.x,p.y,this.turret.rotation, -70);
						p.rotate(p.x, p.y, this.turret.rotation,false,-90);
					}			
					this.crosshair.x = p.x;
					this.crosshair.y = p.y;
				}
			} 
			if(this.weaponButton.isDown && this.weaponPanelTimer < this.game.time.now){
				if(this.game.paused){
					this.changeWeapon();
				}
				else {
					this.showWeaponPanel();					
				}
				this.weaponPanelTimer = this.game.time.now + 650;
			}		
		},
		
		changeWorm: function(){
			this.activeWorm.resetShotsTaken();
			this.activeWorm.setLastWeapon(this.selectedWeapon.getId());
			this.activeWorm = this.turnSystem.progressTurn();
			this.selectedWeapon.setShotsInProgress(false);
			this.weaponPanel.enable();
			if (this.activeWorm == false){
				//this.game.paused = true;
				//********** START END GAME STATE ******************
				console.clear();
				console.log("GAME OVER");
			}
			console.log("wormChanged");
			this.game.camera.follow(this.activeWorm.body);
			this.turret.destroy();
			this.selectedWeapon = this.arsenal.getWeapon(this.activeWorm.getLastWeapon());
			this.crosshair.destroy();
			this.createTurret();
			
			this.timeStamp = this.game.time.now;
		},
		
		doExplosion: function(){
			//this.hitOnce = true;
			this.bullet.kill();
			this.bullet.destroy();
			this.bulletHitLand = false;	
			var oldVertices = this.groundVertices;
			var p = new Phaser.Point(this.bullet.x, this.bullet.y);

			/* this.explosion = new Phaser.Circle(253, 249, 100);			
			var newCirc = this.createCircle(20, new Phaser.Point(253,249), 50); */
			this.explosion = new Phaser.Circle(p.x,p.y, this.selectedWeapon.getExplosionRaidus());			
			var newCirc = this.createCircle(20, p, this.selectedWeapon.getExplosionRaidus()/2);
			var subj_paths = new ClipperLib.Paths();
			
			for(var x = 0; x < this.currArrs.length; x++){
				var subj_path = new ClipperLib.Path();
				var toPush = this.currArrs[x];
				for (var i = 0; i< toPush.length; i+=2){
					subj_path.push(
						new ClipperLib.IntPoint(toPush[i], toPush[i+1]));
				}		
				subj_paths.push(subj_path);
				subj_path = null;
			}

			var clip_paths = new ClipperLib.Paths();
			var clip_path = new ClipperLib.Path();
			for(i=0; i < newCirc.length; i+=2){
				clip_path.push(
					new ClipperLib.IntPoint(Math.round(newCirc[i]), Math.round(newCirc[i+1])));
			}
			clip_paths.push(clip_path);
			var cpr = new ClipperLib.Clipper();
			
			var scale = 1;
			ClipperLib.JS.ScaleUpPaths(subj_paths, scale);
			ClipperLib.JS.ScaleUpPaths(clip_paths, scale);
			
			cpr.AddPaths(subj_paths, ClipperLib.PolyType.ptSubject, true);
			cpr.AddPaths(clip_paths, ClipperLib.PolyType.ptClip, true);
			var solution_paths =[];
			var clipType = ClipperLib.ClipType.ctDifference;
			var subject_fillType = ClipperLib.PolyFillType.pftNonZero;
			var clip_fillType = ClipperLib.PolyFillType.pftNonZero;
			var succeeded = cpr.Execute(clipType, solution_paths, subject_fillType, clip_fillType);
		
			var newBods = [];
			var newArrs = [];
			var toX, toY;
			if(!this.hitOnce){
				for (i = 0; i < solution_paths.length; i++){
					var newTerrain = [];
					var poly = solution_paths[i];
					for(var j = 0; j < poly.length; j++){
						var vert = poly[j];
						if (j == 0){toX =vert.X; toY = vert.Y;}
						newTerrain.push(vert.X);
						newTerrain.push(vert.Y);	
					}
					newTerrain.push(toX);
					newTerrain.push(toY);
					
					newBods.push(this.groundBody.addChain(newTerrain));	
					newArrs.push(newTerrain);
				}
				
				for (i = 0; i < this.currBods.length; i++){
					this.groundBody.removeFixture(this.currBods[i]);
				}
				this.currBods.splice(0, this.currBods.length);
				this.currArrs.splice(0, this.currArrs.length);
				for (i = 0; i < newBods.length; i++){
					this.currBods.push(newBods[i]);
				} 
				for (i = 0; i < newArrs.length; i++){
					this.currArrs.push(newArrs[i]);
				}	
				this.groundBody.setCollisionCategory(2);
				
				var x = Math.floor(this.bullet.x);
				var y = Math.floor(this.bullet.y);
				var rgba = this.land.getPixel(x, y);
				
				this.land.blendDestinationOut();
				this.land.circle(x, y, this.selectedWeapon.getExplosionRaidus()/2, 'rgba(0, 0, 0, 255');
				this.land.blendReset();
				this.land.update();
				this.land.addToWorld();			
				//MAX DX = 600 -> -600; MAX DY  = 700 -> -700;
				for (var i = 0; i < this.turnSystem.getTeams().length; i++){
					for (var j = 0; j < this.turnSystem.getTeams()[i].getWholeTeam().length; j++){
						//console.log("WIDTH: " + this.turnSystem.getTeams()[i].getWholeTeam()[j].getHeight());
						var topLeftY = this.turnSystem.getTeams()[i].getWholeTeam()[j].getY() - (this.turnSystem.getTeams()[i].getWholeTeam()[j].getHeight()/2);
						var topLeftX = this.turnSystem.getTeams()[i].getWholeTeam()[j].getX()-(this.turnSystem.getTeams()[i].getWholeTeam()[j].getWidth()/2);
						var width = this.turnSystem.getTeams()[i].getWholeTeam()[j].getWidth();
						var height = this.turnSystem.getTeams()[i].getWholeTeam()[j].getHeight();
						var lines = [
								new Phaser.Line(topLeftX, topLeftY, topLeftX + width, topLeftY),
								new Phaser.Line(topLeftX, topLeftY,topLeftX, topLeftY+height),
								new Phaser.Line(topLeftX + width, topLeftY, topLeftX+width, topLeftY+height),
								new Phaser.Line(topLeftX, topLeftY + height, topLeftX+width, topLeftY+height)
								];
							var intersects;
							console.log("explosion: " + this.explosion.x + ", "+ this.explosion.y + ".... radius: " + this.explosion.radius);
							for(var p = 0; p < lines.length; p++) {
								var ar = lines[p].coordinatesOnLine();
								this.game.debug.geom(lines[p],'rgba(255,255,0, 1)');
								console.log(p+ ":");
								for (var x = 0; x < ar.length; x++){
									console.log(ar[x][0] + ", " + ar[x][1]);
									intersects = this.explosion.contains(ar[x][0], ar[x][1]);
														//	console.log("intersects");
									//console.log("intersects" + intersects);
									if (intersects) {
										console.log("YEA BITCH");
										break;
									}
								}
								if (intersects) {
									break;
								}
							}
									// Find the closest intersection
									//dist = this.game.math.distance(this.explosion.x, this.explosion.y, intersect.x, intersect.y);
								/* }
								
					}						if (this.explosion.contains(this.turnSystem.getTeams()[i].getWholeTeam()[j].getX(), this.turnSystem.getTeams()[i].getWholeTeam()[j].getY())){ */
							//console.log("WORM IN EXPLOSION");
							if (intersects){
							var dx =  this.turnSystem.getTeams()[i].getWholeTeam()[j].getX()-this.explosion.x ;
							var dy = this.turnSystem.getTeams()[i].getWholeTeam()[j].getY()-this.explosion.y  ;
							//console.log(dx + ", " + dy);
							dx*=30;dy*=30;	
							//dx-=();'
							//dx*=this.firePower/10;
							//console.log("FirepwerL: " + this.firePower);
							//dy*=this.firePower/10;
							//dx = 600; dy =-700;
							this.firePower = 0;
						/* var lines = [
								new Phaser.Line(this.turnSystem.getTeams()[i].getWholeTeam()[j].getX(), this.turnSystem.getTeams()[i].getWholeTeam()[j].getY(), this.turnSystem.getTeams()[i].getWholeTeam()[j].getX() + this.turnSystem.getTeams()[i].getWholeTeam()[j].getWidth(), this.turnSystem.getTeams()[i].getWholeTeam()[j].getY()),
								new Phaser.Line(this.turnSystem.getTeams()[i].getWholeTeam()[j].getX(), this.turnSystem.getTeams()[i].getWholeTeam()[j].getY(), this.turnSystem.getTeams()[i].getWholeTeam()[j].getX(), this.turnSystem.getTeams()[i].getWholeTeam()[j].getY() + this.turnSystem.getTeams()[i].getWholeTeam()[j].getHeight()),
								new Phaser.Line(this.turnSystem.getTeams()[i].getWholeTeam()[j].getX() + this.turnSystem.getTeams()[i].getWholeTeam()[j].getWidth(), this.turnSystem.getTeams()[i].getWholeTeam()[j].getY(),
									this.turnSystem.getTeams()[i].getWholeTeam()[j].getX() + this.turnSystem.getTeams()[i].getWholeTeam()[j].getWidth(), this.turnSystem.getTeams()[i].getWholeTeam()[j].getY() + this.turnSystem.getTeams()[i].getWholeTeam()[j].getHeight()),
								new Phaser.Line(this.turnSystem.getTeams()[i].getWholeTeam()[j].getX(), this.turnSystem.getTeams()[i].getWholeTeam()[j].getY() + this.turnSystem.getTeams()[i].getWholeTeam()[j].getHeight(),
									this.turnSystem.getTeams()[i].getWholeTeam()[j].getX() + this.turnSystem.getTeams()[i].getWholeTeam()[j].getWidth(), this.turnSystem.getTeams()[i].getWholeTeam()[j].getY() + this.turnSystem.getTeams()[i].getWholeTeam()[j].getHeight())
							];
							
							for(var i = 0; i < lines.length; i++) {
								var intersect = Phaser.Line.intersects(this.explosion, lines[i]);
								if (intersect) {
									// Find the closest intersection
									dist = this.game.math.distance(this.explosion.x, this.explosion.y, intersect.x, intersect.y);
								}
							} */
							
				//TEMP - TODO: APPLY CALCULATED DAMAGE, NOT 50 **********************************************			
							this.turnSystem.getTeams()[i].getWholeTeam()[j].applyDamage(50);							
							var dist = this.explosion.distance(new Phaser.Point(this.turnSystem.getTeams()[i].getWholeTeam()[j].getX()-10,this.turnSystem.getTeams()[i].getWholeTeam()[j].getY()-10));
							var distDed = (dist/this.explosion.radius); console.log("distDed: " + distDed);
							//dx-=(dx*distDed);
							//dy-=(dy*distDed);
							
				//TEMP***************************************************************************************
							this.turnSystem.getTeams()[i].getWholeTeam()[j].body.body.velocity.x = dx-(dx*distDed);
							this.turnSystem.getTeams()[i].getWholeTeam()[j].body.body.velocity.y= dy-(dy*distDed);
				//TEMP***************************************************************************************
						}
					}
				}
				this.activeWorm.setShotsTaken(this.activeWorm.getShotsTaken() + 1);
				this.hitOnce = true;
			}			
			this.bulletExists=false;
			this.exploded = true;
			//add delay
			this.game.camera.follow(this.activeWorm.body);			
			this.weaponPanel.bringToFront();
		},
		
		showWeaponPanel : function(){
			this.weaponPanel = new WeaponPanel($(window).width()/2,$(window).height()-15);
			this.weaponPanel.show(this.game);
		},
		
		changeWeapon: function(event){
			if (this.weaponPanel != undefined && !this.selectedWeapon.getShotsInProgress()){
				changeWeaponIndex = this.weaponPanel.resolveClick(event, this.selectedWeapon.getId());
				if (changeWeaponIndex != this.selectedWeapon.getId()){
					this.selectedWeapon = this.arsenal.changeWeapon(changeWeaponIndex);
					this.firePower = 0;
					this.turret.destroy();
					this.crosshair.destroy();
					this.createTurret();
					this.changeTimer+= 650;
				}
			}
		},

		createCircle: function (precision, origin, radius) {
			var angle=2*Math.PI/precision;
			var circleArray = [];
			var multiplier = 0;
			for (var i=0; i<(precision); i++) {
				//circleArray.push(new Phaser.Point(origin.x+radius*Math.cos(angle*i),origin.y+radius*Math.sin(angle*i)));
				multiplier = i*2;
				circleArray[multiplier] =(origin.x+radius*Math.cos(angle*i));
				circleArray[multiplier+1] = (origin.y+radius*Math.sin(angle*i));
			}
			//circleArray.splice(20, 20);
			return circleArray;
		},
		
		render: function () {
			if(this.debugOn){
				this.game.debug.text(this.game.time.fps || '--', 2, 14, "#00ff00");
				this.game.debug.box2dWorld();
				this.game.debug.geom(this.explosion,'rgba(0,255,0, 0.6)');
				this.game.debug.geom(this.turretPointer,'rgba(0,255,0, 0.6)');
			}
			
			//this.game.debug.geom(this.point,'rgb222,18,18, 1)');
			for (i=0; i < this.powerIndicator.length; i++){
			this.game.debug.geom(this.powerIndicator[i],'rgba(' + this.powerCol + ',18,18,1)');}

		},
    };